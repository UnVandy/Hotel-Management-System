/*
 Navicat Premium Data Transfer

 Source Server         : root
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : hotel

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 23/08/2018 12:15:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tblbooking
-- ----------------------------
DROP TABLE IF EXISTS `tblbooking`;
CREATE TABLE `tblbooking`  (
  `boID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `guID` int(10) UNSIGNED NOT NULL,
  `rID` int(10) UNSIGNED NOT NULL,
  `boCheckIn` date NOT NULL,
  `boCheckOut` date NOT NULL,
  `contact` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dns` int(11) NOT NULL,
  PRIMARY KEY (`boID`) USING BTREE,
  INDEX `rID`(`rID`) USING BTREE,
  INDEX `guID`(`guID`) USING BTREE,
  CONSTRAINT `tblbooking_ibfk_2` FOREIGN KEY (`rID`) REFERENCES `tblroom` (`rid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblbooking_ibfk_3` FOREIGN KEY (`guID`) REFERENCES `tblguest` (`guid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblcheckexpire
-- ----------------------------
DROP TABLE IF EXISTS `tblcheckexpire`;
CREATE TABLE `tblcheckexpire`  (
  `chID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `guID` int(10) UNSIGNED NOT NULL,
  `rID` int(10) UNSIGNED NOT NULL,
  `ckCheckIn` date NOT NULL,
  `ckCheckOut` date NOT NULL,
  `contact` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dns` int(11) NOT NULL,
  PRIMARY KEY (`chID`) USING BTREE,
  INDEX `rID`(`rID`) USING BTREE,
  INDEX `guID`(`guID`) USING BTREE,
  CONSTRAINT `tblcheckexpire_ibfk_2` FOREIGN KEY (`rID`) REFERENCES `tblroom` (`rid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblcheckexpire_ibfk_3` FOREIGN KEY (`guID`) REFERENCES `tblguest` (`guid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblcheckin
-- ----------------------------
DROP TABLE IF EXISTS `tblcheckin`;
CREATE TABLE `tblcheckin`  (
  `ckID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `guID` int(10) UNSIGNED NOT NULL,
  `rID` int(11) UNSIGNED NOT NULL,
  `chCheckIn` date NOT NULL,
  `chCheckOut` date NOT NULL,
  `contact` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dns` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`ckID`) USING BTREE,
  INDEX `rID`(`rID`) USING BTREE,
  INDEX `guID`(`guID`) USING BTREE,
  CONSTRAINT `tblcheckin_ibfk_1` FOREIGN KEY (`rID`) REFERENCES `tblroom` (`rid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblcheckin_ibfk_2` FOREIGN KEY (`guID`) REFERENCES `tblguest` (`guid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblcheckout
-- ----------------------------
DROP TABLE IF EXISTS `tblcheckout`;
CREATE TABLE `tblcheckout`  (
  `guID` int(10) UNSIGNED NOT NULL,
  `guName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `guAdd` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rID` int(10) UNSIGNED NOT NULL,
  `rtID` int(11) NULL DEFAULT NULL,
  `roPrice` double(11, 0) NOT NULL,
  `roSize` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `checkIn` date NOT NULL,
  `checkOut` date NOT NULL,
  `contact` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dns` int(11) NULL DEFAULT NULL,
  `prepaid` double NULL DEFAULT NULL,
  `amount` double NOT NULL,
  `idran` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`guID`) USING BTREE,
  INDEX `guID`(`guID`) USING BTREE,
  INDEX `rID`(`rID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tblcheckout
-- ----------------------------
INSERT INTO `tblcheckout` VALUES (41, 'boca', '93939', 'takoe\n', 108, 18, 200, 'Twice', '2017-04-01', '2017-04-01', '93939', 1, 22, 200, '87238641');
INSERT INTO `tblcheckout` VALUES (42, 'sina', '9393', 'takeo', 126, 21, 70, 'Single', '2017-04-01', '2017-04-02', '999339', 1, 100, 70, '26974366');
INSERT INTO `tblcheckout` VALUES (45, 'borey', '83838', 'takoe', 130, 21, 70, 'Single', '2017-04-01', '2017-04-04', '939339', 3, 120, 210, '39249535');
INSERT INTO `tblcheckout` VALUES (46, 'total', '939393393', 'takeo\n', 79, 22, 50, 'Double', '2017-04-02', '2017-04-03', '9234433443', 1, 233, 50, '60278762');
INSERT INTO `tblcheckout` VALUES (57, 'chenda', '200', 'takeo', 91, 17, 1000, 'Single', '2017-04-19', '2017-04-21', '012112233', 2, 0, 2000, '32796898');
INSERT INTO `tblcheckout` VALUES (60, 'un vandeth', '111111111', 'Phnom penh', 131, 21, 70, 'Single', '2017-04-08', '2017-04-12', '0121122331', 4, 12, 268, '40813084');
INSERT INTO `tblcheckout` VALUES (62, 'Phannara', '213211224', 'Phnom Penh\n', 65, 22, 200, 'Twice', '2017-04-12', '2017-04-15', '010679798', 3, 100, 400, '83444879');
INSERT INTO `tblcheckout` VALUES (63, 'vandy', '222222222', 'takeo', 126, 21, 70, 'Single', '2017-04-12', '2017-04-14', '010679798', 2, 0, 140, '24874032');
INSERT INTO `tblcheckout` VALUES (67, 'phannara', '933333333', 'takeo', 107, 18, 200, 'Twice', '2017-04-13', '2017-04-15', '9333333333', 2, 333, 67, '9013440');
INSERT INTO `tblcheckout` VALUES (68, 'vandy', '939394949', 'takeo', 87, 17, 1000, 'Single', '2017-04-13', '2017-04-17', '0121212121', 4, 200, 3800, '18966068');
INSERT INTO `tblcheckout` VALUES (69, 'eerer', '499499', 'kfjsdlf;\n', 79, 22, 50, 'Double', '2017-04-15', '2017-04-15', '300304', 1, 0, 50, '81300631');
INSERT INTO `tblcheckout` VALUES (70, 'Panha', '11112222', 'PP', 62, 17, 1000, 'Single', '2017-04-13', '2017-04-14', '0978585848', 1, 90, 910, '703656');
INSERT INTO `tblcheckout` VALUES (71, 'Kiry', '111', 'PP	', 124, 21, 70, 'Single', '2017-04-13', '2017-04-13', '097996999', 1, 0, 70, '42568339');
INSERT INTO `tblcheckout` VALUES (72, 'Houn socheatpanha', '1234', 'pp', 81, 22, 50, 'Double', '2017-04-18', '2017-04-20', '0969261828', 2, 20, 80, '80736860');
INSERT INTO `tblcheckout` VALUES (73, 'James', '2222', 'pp	', 73, 18, 200, 'Twice', '2017-04-19', '2017-04-20', '096', 1, 90, 110, '17901346');
INSERT INTO `tblcheckout` VALUES (74, 'vandy', '4444', 'pp', 90, 17, 1000, 'Single', '2017-04-18', '2017-04-19', '0969261838', 1, 0, 1000, '18892511');
INSERT INTO `tblcheckout` VALUES (75, 'vandy', '4444', 'pp', 89, 17, 1000, 'Single', '2017-04-18', '2017-04-19', '0969261838', 1, 0, 1000, '18892511');
INSERT INTO `tblcheckout` VALUES (76, 'socheat', '222244444', 'pp', 71, 22, 50, 'Double', '2017-04-20', '2017-04-22', '0969261828', 2, 10, 90, '80354329');

-- ----------------------------
-- Table structure for tblguest
-- ----------------------------
DROP TABLE IF EXISTS `tblguest`;
CREATE TABLE `tblguest`  (
  `guID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `guName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Noname',
  `idc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `guAdd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rID` int(10) UNSIGNED NOT NULL,
  `rtID` int(10) UNSIGNED NOT NULL,
  `roPrice` double NOT NULL,
  `roSize` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `checkIn` date NOT NULL,
  `checkOut` date NOT NULL,
  `contact` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dns` int(11) NOT NULL,
  `prepaid` double NULL DEFAULT 0,
  `amount` double NOT NULL,
  `idran` int(255) NOT NULL,
  PRIMARY KEY (`guID`) USING BTREE,
  INDEX `rtID`(`rtID`) USING BTREE,
  INDEX `rID`(`rID`) USING BTREE,
  CONSTRAINT `tblguest_ibfk_1` FOREIGN KEY (`rtID`) REFERENCES `tblroomtype` (`rtid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tblguest_ibfk_2` FOREIGN KEY (`rID`) REFERENCES `tblroom` (`rid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 77 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tblguest
-- ----------------------------
INSERT INTO `tblguest` VALUES (56, 'chenda', '200', 'takeo', 70, 17, 1000, 'Single', '2017-04-14', '2017-04-27', '012112233', 13, 12, 12988, 32796898);

-- ----------------------------
-- Table structure for tblincome
-- ----------------------------
DROP TABLE IF EXISTS `tblincome`;
CREATE TABLE `tblincome`  (
  `inID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `inDate` date NOT NULL,
  `stID` int(10) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  PRIMARY KEY (`inID`) USING BTREE,
  INDEX `stID`(`stID`) USING BTREE,
  CONSTRAINT `tblincome_ibfk_2` FOREIGN KEY (`stID`) REFERENCES `tbstaff` (`stid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tblroom
-- ----------------------------
DROP TABLE IF EXISTS `tblroom`;
CREATE TABLE `tblroom`  (
  `rID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `rName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rStatue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rtID` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`rID`) USING BTREE,
  INDEX `rtID`(`rtID`) USING BTREE,
  CONSTRAINT `tblroom_ibfk_1` FOREIGN KEY (`rtID`) REFERENCES `tblroomtype` (`rtid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 135 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tblroom
-- ----------------------------
INSERT INTO `tblroom` VALUES (62, 'vip-S001', 'Free', 17);
INSERT INTO `tblroom` VALUES (63, 'vip-S002', 'Free', 17);
INSERT INTO `tblroom` VALUES (65, 'vip-B004', 'Free', 22);
INSERT INTO `tblroom` VALUES (68, 'n-M003', 'Free', 18);
INSERT INTO `tblroom` VALUES (69, 'n-B004', 'Free', 20);
INSERT INTO `tblroom` VALUES (70, 'vip-S005', 'Checkin', 17);
INSERT INTO `tblroom` VALUES (71, 'vip-B006', 'Free', 22);
INSERT INTO `tblroom` VALUES (72, 'n-B007', 'Free', 20);
INSERT INTO `tblroom` VALUES (73, 'N-M008', 'Free', 18);
INSERT INTO `tblroom` VALUES (75, 'vip-B009', 'Free', 22);
INSERT INTO `tblroom` VALUES (77, 'vip-B101', 'Free', 22);
INSERT INTO `tblroom` VALUES (78, 'vip-B102', 'Free', 22);
INSERT INTO `tblroom` VALUES (79, 'vip-B103', 'Free', 22);
INSERT INTO `tblroom` VALUES (80, 'vip-B104', 'Free', 22);
INSERT INTO `tblroom` VALUES (81, 'vip-B105', 'Free', 22);
INSERT INTO `tblroom` VALUES (82, 'vip-B106', 'Free', 22);
INSERT INTO `tblroom` VALUES (83, 'vip-B107', 'Free', 22);
INSERT INTO `tblroom` VALUES (84, 'vip-B108', 'Free', 22);
INSERT INTO `tblroom` VALUES (85, 'vip-B109', 'Free', 22);
INSERT INTO `tblroom` VALUES (86, 'vip-S101', 'Free', 17);
INSERT INTO `tblroom` VALUES (87, 'vip-S102', 'Free', 17);
INSERT INTO `tblroom` VALUES (88, 'vip-S103', 'Free', 17);
INSERT INTO `tblroom` VALUES (89, 'vip-S104', 'Free', 17);
INSERT INTO `tblroom` VALUES (90, 'vip-S105', 'Free', 17);
INSERT INTO `tblroom` VALUES (91, 'vip-S106', 'Free', 17);
INSERT INTO `tblroom` VALUES (92, 'vip-S107', 'Free', 17);
INSERT INTO `tblroom` VALUES (93, 'vip-S108', 'Free', 17);
INSERT INTO `tblroom` VALUES (94, 'vip-S109', 'Free', 17);
INSERT INTO `tblroom` VALUES (106, 'N-M102', 'Free', 18);
INSERT INTO `tblroom` VALUES (107, 'N-M103', 'Free', 18);
INSERT INTO `tblroom` VALUES (108, 'N-M104', 'Free', 18);
INSERT INTO `tblroom` VALUES (109, 'N-M105', 'Free', 18);
INSERT INTO `tblroom` VALUES (110, 'N-M106', 'Free', 18);
INSERT INTO `tblroom` VALUES (111, 'N-M107', 'Free', 18);
INSERT INTO `tblroom` VALUES (112, 'N-M108', 'Free', 18);
INSERT INTO `tblroom` VALUES (113, 'N-M109', 'Free', 18);
INSERT INTO `tblroom` VALUES (114, 'N-B102', 'Free', 20);
INSERT INTO `tblroom` VALUES (115, 'N-B103', 'Free', 20);
INSERT INTO `tblroom` VALUES (116, 'N-B201', 'Free', 20);
INSERT INTO `tblroom` VALUES (117, 'N-B104', 'Free', 20);
INSERT INTO `tblroom` VALUES (118, 'N-B105', 'Free', 20);
INSERT INTO `tblroom` VALUES (119, 'N-B106', 'Free', 20);
INSERT INTO `tblroom` VALUES (120, 'N-B107', 'Free', 20);
INSERT INTO `tblroom` VALUES (121, 'N-B108', 'Free', 20);
INSERT INTO `tblroom` VALUES (122, 'N-B109', 'Free', 20);
INSERT INTO `tblroom` VALUES (134, 'vip-s000', 'Free', 17);

-- ----------------------------
-- Table structure for tblroomtype
-- ----------------------------
DROP TABLE IF EXISTS `tblroomtype`;
CREATE TABLE `tblroomtype`  (
  `rtID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `rtType` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rtSize` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `rtPrice` double NOT NULL,
  `rtDes` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`rtID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tblroomtype
-- ----------------------------
INSERT INTO `tblroomtype` VALUES (17, 'VIP-S', 'Single', 1000, 'floor5');
INSERT INTO `tblroomtype` VALUES (18, 'Normal-M', 'Twice', 200, 'floor6');
INSERT INTO `tblroomtype` VALUES (20, 'Normal-B', 'Double', 150, 'floor4');
INSERT INTO `tblroomtype` VALUES (22, 'VIP-B', 'Double', 50, 'floor1');
INSERT INTO `tblroomtype` VALUES (23, 'VIP-B2', 'double', 200, 'floor5');

-- ----------------------------
-- Table structure for tbluser
-- ----------------------------
DROP TABLE IF EXISTS `tbluser`;
CREATE TABLE `tbluser`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `stid` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `stid`(`stid`) USING BTREE,
  CONSTRAINT `tbluser_ibfk_1` FOREIGN KEY (`stid`) REFERENCES `tbstaff` (`stid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbluser
-- ----------------------------
INSERT INTO `tbluser` VALUES (9, 11, 'vandy', '1234');
INSERT INTO `tbluser` VALUES (27, 31, 'thearith', '1234');
INSERT INTO `tbluser` VALUES (28, 32, 'socheat', '123');
INSERT INTO `tbluser` VALUES (29, 10, 'chanda', '1');
INSERT INTO `tbluser` VALUES (30, 30, 'acbd', '1234');

-- ----------------------------
-- Table structure for tbstaff
-- ----------------------------
DROP TABLE IF EXISTS `tbstaff`;
CREATE TABLE `tbstaff`  (
  `stID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `stName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Sex` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dob` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `stPos` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Salary` float NOT NULL,
  `stAdd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `stPhone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `HiredDate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `StopWork` bigint(20) NULL DEFAULT NULL,
  `Photo` longblob NULL,
  PRIMARY KEY (`stID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbstaff
-- ----------------------------
INSERT INTO `tbstaff` VALUES (10, 'chanda', 'M', '2017-03-23', 'securty guard', 100, 'takeo', '333', '2017-03-23', 1, 0xEFBFBD504E470D0A1A0A);
INSERT INTO `tbstaff` VALUES (11, 'vandy', 'M', '2017-03-30', 'Manager', 2007, 'takeo', '010679798', '2017-03-30', 1, 0xEFBFBD504E470D0A1A0A);
INSERT INTO `tbstaff` VALUES (23, 'tota', 'F', '2017-03-18', 'manager', 2, 'takeo', '292939', '2017-03-18', 1, 0xEFBFBD504E470D0A1A0A);
INSERT INTO `tbstaff` VALUES (25, 'sina', 'M', '2017-03-23', 'fkjfs', 12000, 'kjsfj', '94949', '2017-03-23', 0, 0xEFBFBD504E470D0A1A0A);
INSERT INTO `tbstaff` VALUES (30, 'pisey', 'F', '2017-04-02', 'Cashi', 150, 'phnom penh', '012112233', '2017-04-02', 1, 0xEFBFBD504E470D0A1A0A);
INSERT INTO `tbstaff` VALUES (31, 'thearith', 'M', '2017-04-13', 'Driver', 140, 'Phnom Penh', '011221133', '2017-04-18', 1, 0xEFBFBD504E470D0A1A0A);
INSERT INTO `tbstaff` VALUES (32, 'socheat', 'F', '2017-04-12', 'staff', 400, 'PP', '0969261828', '2017-04-12', 1, 0xEFBFBD504E470D0A1A0A);

-- ----------------------------
-- View structure for vcheckout
-- ----------------------------
DROP VIEW IF EXISTS `vcheckout`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `vcheckout` AS select `tblcheckout`.`guName` AS `guName`,`tblcheckout`.`idc` AS `idc`,`tblcheckout`.`guAdd` AS `guAdd`,`tblcheckout`.`roPrice` AS `roPrice`,`tblcheckout`.`roSize` AS `roSize`,`tblcheckout`.`checkIn` AS `checkIn`,`tblcheckout`.`checkOut` AS `checkOut`,`tblcheckout`.`contact` AS `contact`,`tblcheckout`.`dns` AS `dns`,`tblcheckout`.`prepaid` AS `prepaid`,`tblcheckout`.`amount` AS `amount`,`tblroom`.`rName` AS `rName`,`tblroomtype`.`rtType` AS `rtType`,`tblcheckout`.`guID` AS `guID`,`tblroomtype`.`rtSize` AS `rtSize`,`tblroom`.`rStatue` AS `rStatue`,`tblroomtype`.`rtPrice` AS `rtPrice`,`tblroomtype`.`rtDes` AS `rtDes`,`tblcheckout`.`idran` AS `idran` from ((`tblcheckout` join `tblroom` on((`tblcheckout`.`rID` = `tblroom`.`rID`))) join `tblroomtype` on((`tblcheckout`.`rtID` = `tblroomtype`.`rtID`)));

-- ----------------------------
-- View structure for vguestdetail
-- ----------------------------
DROP VIEW IF EXISTS `vguestdetail`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `vguestdetail` AS select `tblguest`.`guName` AS `guName`,`tblguest`.`idc` AS `idc`,`tblguest`.`contact` AS `contact`,`tblguest`.`guAdd` AS `guAdd`,`tblroom`.`rName` AS `rName`,`tblroomtype`.`rtType` AS `rtType`,`tblroomtype`.`rtSize` AS `rtSize`,`tblroomtype`.`rtPrice` AS `rtPrice`,`tblroomtype`.`rtDes` AS `rtDes`,`tblguest`.`checkIn` AS `checkIn`,`tblguest`.`checkOut` AS `checkOut`,`tblguest`.`prepaid` AS `prepaid`,`tblguest`.`amount` AS `amount`,`tblguest`.`idran` AS `idran`,`tblguest`.`guID` AS `guID`,`tblguest`.`dns` AS `dns`,`tblroom`.`rStatue` AS `rStatue` from ((`tblguest` join `tblroom` on(((`tblguest`.`rID` = `tblroom`.`rID`) and (`tblguest`.`rtID` = `tblroom`.`rtID`)))) join `tblroomtype` on((`tblroom`.`rtID` = `tblroomtype`.`rtID`)));

-- ----------------------------
-- View structure for vroomdetail
-- ----------------------------
DROP VIEW IF EXISTS `vroomdetail`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `vroomdetail` AS select `tblguest`.`rID` AS `rID`,`tblguest`.`checkIn` AS `checkIn`,`tblguest`.`checkOut` AS `checkOut`,`tblroom`.`rStatue` AS `rStatue`,`tblroom`.`rtID` AS `rtID` from (`tblguest` join `tblroom` on((`tblguest`.`rID` = `tblroom`.`rID`)));

-- ----------------------------
-- View structure for vroomtypedetail
-- ----------------------------
DROP VIEW IF EXISTS `vroomtypedetail`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `vroomtypedetail` AS select `tblroomtype`.`rtType` AS `rtType`,`tblroomtype`.`rtSize` AS `rtSize`,`tblroom`.`rName` AS `rName`,`tblroom`.`rStatue` AS `rStatue`,`tblroom`.`rID` AS `rID`,`tblroom`.`rtID` AS `rtID`,`tblroomtype`.`rtPrice` AS `rtPrice` from (`tblroomtype` join `tblroom` on((`tblroom`.`rtID` = `tblroomtype`.`rtID`)));

SET FOREIGN_KEY_CHECKS = 1;
