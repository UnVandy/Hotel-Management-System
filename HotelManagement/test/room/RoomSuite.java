/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package room;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author DELL
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({room.ConnectDBTest.class, room.CRoomTest.class, room.CheckInTest.class, room.UserTest.class, room.CGuestTest.class, room.MyContecionTest.class, room.startFormTest.class, room.GuestTest.class, room.RoomTest.class, room.ShowUserTest.class, room.BackGroundFormTest.class, room.TestTest.class, room.CheckOutTest.class, room.MoveTextTest.class, room.RoomTypeTest.class, room.AboutUsTest.class, room.BookingTest.class, room.PaymentTest.class, room.DataConllectionTest.class, room.StaffTest.class, room.CStaffTest.class, room.LoginTest.class, room.Picture.PictureSuite.class, room.CRoomTypeTest.class, room.MainFormTest.class})
public class RoomSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
