package room;

import java.sql.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.*;
import java.util.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class Guest extends javax.swing.JFrame {
    private Statement st;
    private ResultSet r;
    public String getName;
    private FocusEvent evt;
    public static int bb=0;
    
    public Guest() {
        initComponents();
        setLocationRelativeTo(null);
        addCombo();
        myStatue();
        bb=0;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jRadioButton1 = new javax.swing.JRadioButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        cboType = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtSize = new javax.swing.JTextField();
        txtDay = new javax.swing.JTextField();
        txtPrice = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtFloor = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtPaid = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txtAmount = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        dd = new javax.swing.JLabel();
        txtGTotal = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtRID = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        cboRName = new javax.swing.JComboBox<>();
        jLabel22 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtAddress = new javax.swing.JTextArea();
        txtContact = new jnumberfield.JNumberField();
        txtCard = new jnumberfield.JNumberField();
        txtPrepaid = new javax.swing.JTextField();
        dCheckIn = new com.toedter.calendar.JDateChooser();
        dCheckOut = new com.toedter.calendar.JDateChooser();
        jPanel4 = new javax.swing.JPanel();
        lblDate = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        lblTime = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        btnDelete = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnNew = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        btnPayment = new javax.swing.JButton();
        txtID = new javax.swing.JTextField();

        jRadioButton1.setText("jRadioButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel2.setBackground(new java.awt.Color(153, 255, 153));

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel10.setText("Stay In:");

        txtName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNameKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNameKeyTyped(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel9.setText("CheckOut:");

        cboType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTypeActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel13.setText("Room Type:");

        txtTotal.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtTotal.setFocusable(false);

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel8.setText("Room Size:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Customer Name:");

        txtSize.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtSize.setFocusable(false);

        txtDay.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtDay.setFocusable(false);

        txtPrice.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtPrice.setFocusable(false);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Room Price:");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel11.setText("Total:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("CheckIn:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Contact:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Prepaid:");

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel12.setText("Identify Card:");

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel15.setText("Room Name:");

        txtFloor.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtFloor.setFocusable(false);

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel16.setText("Floor:");

        txtPaid.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtPaid.setFocusable(false);

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel17.setText("Paid:");

        txtAmount.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtAmount.setText("0");
        txtAmount.setFocusable(false);

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel18.setText("Amount:");

        dd.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        dd.setText("G-Total:");

        txtGTotal.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtGTotal.setFocusable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 0, 51));
        jLabel1.setText("Customer Information");

        jLabel7.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 0, 0));
        jLabel7.setText("Room Information");

        txtRID.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtRID.setFocusable(false);

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel19.setText("ID:");

        cboRName.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cboRNameMouseClicked(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel22.setText("Address:");

        txtAddress.setColumns(20);
        txtAddress.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtAddress.setRows(5);
        jScrollPane1.setViewportView(txtAddress);

        txtContact.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtContact.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtContactKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtContactKeyTyped(evt);
            }
        });

        txtCard.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtCard.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCardKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCardKeyTyped(evt);
            }
        });

        txtPrepaid.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtPrepaid.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtPrepaidFocusGained(evt);
            }
        });
        txtPrepaid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrepaidActionPerformed(evt);
            }
        });
        txtPrepaid.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrepaidKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addGap(22, 22, 22)
                            .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel22))
                        .addGap(22, 22, 22)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtContact, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCard, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
                                    .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtFloor, javax.swing.GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
                                    .addComponent(txtPrice)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(dCheckIn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(dCheckOut, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel17)
                                    .addComponent(jLabel18)
                                    .addComponent(dd))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtGTotal)
                                    .addComponent(txtAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
                                    .addComponent(txtPaid)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(18, 18, 18)
                                .addComponent(txtPrepaid))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(cboType, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(cboRName, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(txtSize, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel19)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                    .addComponent(jLabel11)
                                    .addGap(18, 18, 18))
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(jLabel10)
                                    .addGap(18, 18, 18))))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtDay, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtRID, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
                            .addComponent(txtTotal))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel12, jLabel2, jLabel5});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {dd, jLabel10, jLabel11, jLabel17, jLabel18, jLabel6});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cboType, txtFloor});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel1)
                    .addComponent(txtRID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(cboType))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(txtPrepaid, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel17)
                                    .addComponent(txtPaid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel18)
                                    .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(dd)
                                    .addComponent(txtGTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel3)
                                    .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtFloor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel16))
                                .addGap(17, 17, 17)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(dCheckIn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(jLabel9))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(dCheckOut, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11)
                            .addComponent(cboRName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtSize)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(txtCard, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel5)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(txtContact, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(1, 1, 1)))
                                .addGap(18, 18, 18)
                                .addComponent(jLabel22)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addComponent(jScrollPane1)))))
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cboRName, cboType, jLabel13, jLabel15, jLabel16, jLabel3, jLabel8, txtDay, txtFloor, txtPrice, txtSize, txtTotal});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel12, jLabel2, jLabel5, txtCard, txtContact, txtName});

        jPanel4.setBackground(new java.awt.Color(153, 153, 255));

        lblDate.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblDate.setText("date");

        btnBack.setBackground(new java.awt.Color(255, 255, 255));
        btnBack.setForeground(new java.awt.Color(255, 51, 51));
        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/Arrows-Undo-icon.png"))); // NOI18N
        btnBack.setContentAreaFilled(false);
        btnBack.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnBackMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnBackMouseExited(evt);
            }
        });
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        lblTime.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        lblTime.setText("time");

        jLabel20.setFont(new java.awt.Font("Segoe Print", 3, 36)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(153, 0, 0));
        jLabel20.setText("Hotel Managerment");

        jLabel21.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel21.setText("Date:");

        txtSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSearch.setForeground(new java.awt.Color(204, 204, 204));
        txtSearch.setText("Search name,contact,card");
        txtSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtSearch.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtSearchFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtSearchFocusLost(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(155, 155, 155)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblDate, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTime, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel20, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTime)
                            .addComponent(lblDate)
                            .addComponent(jLabel21))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnBack))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(153, 153, 255));

        btnDelete.setBackground(new java.awt.Color(255, 255, 255));
        btnDelete.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/Actions-edit-delete-icon.png"))); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDeleteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnDeleteMouseExited(evt);
            }
        });
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnEdit.setBackground(new java.awt.Color(255, 255, 255));
        btnEdit.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/Edit-validated-icon.png"))); // NOI18N
        btnEdit.setText("Edit");
        btnEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEditMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnEditMouseExited(evt);
            }
        });
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/Save-icon.png"))); // NOI18N
        btnSave.setText("Save");
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveMouseExited(evt);
            }
        });
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnNew.setBackground(new java.awt.Color(255, 255, 255));
        btnNew.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        btnNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/Folder-Add-icon.png"))); // NOI18N
        btnNew.setText("New");
        btnNew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnNewMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnNewMouseExited(evt);
            }
        });
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });

        table.setForeground(new java.awt.Color(0, 51, 153));
        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(table);

        btnPayment.setBackground(new java.awt.Color(255, 255, 255));
        btnPayment.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        btnPayment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/Apps-system-software-update-icon.png"))); // NOI18N
        btnPayment.setText("Payment");
        btnPayment.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPaymentMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPaymentMouseExited(evt);
            }
        });
        btnPayment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPaymentActionPerformed(evt);
            }
        });

        txtID.setFocusable(false);
        txtID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIDActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPayment, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(198, 198, 198))
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnEdit, btnNew, btnSave});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(12, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSave)
                        .addComponent(btnNew)
                        .addComponent(btnEdit)
                        .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnPayment, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE))
                    .addComponent(txtID, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnDelete, btnEdit, btnNew, btnPayment, btnSave});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

   
     private void formLoad(){
        myDateTime();
        initTable();
        myBackup();
        myClear();
        noCusor();       
        txtID.setVisible(false);
        buttonOff();
    }
    private void buttonOff(){
        btnSave.setText("Save");
        btnSave.setEnabled(false);
        btnEdit.setText("Edit");
        btnEdit.setEnabled(false);
        btnDelete.setEnabled(false);
    }
    private void buttonOn(String b1){
        btnSave.setEnabled(true);
        btnEdit.setEnabled(true);
        btnDelete.setEnabled(true);
        btnSave.setText("Save");
        btnEdit.setText("Edit");
    }
     
     private void getNew(){
        if(btnNew.getText()=="New"){
           // btnNew.setIcon(new ImageIcon(.getClass().getResource("book/picture/cancel.jpg")));
            txtSearch.setForeground(Color.BLACK);
            while(model.getRowCount()>0){
                        for(int j=0;j<model.getRowCount();j++)
                        model.removeRow(j);
            }
            btnNew.setIcon(new ImageIcon(getClass().getResource("Picture\\Cancel.png"))); 
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png")));
            getCusor();
            btnNew.setText("Cancel");
            myClear();
            buttonOff();
            btnSave.setEnabled(true);
            myRandom();
        }else if(btnNew.getText()=="Cancel"){
            int a=JOptionPane.showConfirmDialog(null,"Do you want to cancel ?","Comfirm",JOptionPane.YES_NO_OPTION);
            if(JOptionPane.YES_OPTION==a){
            btnNew.setIcon(new ImageIcon(getClass().getResource("Picture\\Folder-Add-icon.png")));
            myClear();
            noCusor();
            btnNew.setText("New");
            btnSave.setEnabled(false);
            }
        }
    }
    private void getEdit(){
        if(btnEdit.getText()=="Edit"){
            getCusor();
            //cboRName.setSelectedItem(""+model.getValueAt(table.getSelectedRow(), 5));
            btnSave.setText("Update");
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Cancel.png"))); 
            btnEdit.setText("Cancel");
            btnSave.setEnabled(true);
            cboRName.setSelectedItem(null);
            cboRName.addItem(""+model.getValueAt(table.getSelectedRow(), 5));
        }else if(btnEdit.getText()=="Cancel"){
            myClear();
            noCusor();
            btnEdit.setText("Edit");
            btnSave.setText("Save");
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png"))); 
            buttonOff();
        }
    }
    private void getUpdate(){
        if(btnSave.getText()=="Save"){
            mySave();
            toTable();
        }else if(btnSave.getText()=="Update"){
            myUpdate();            
            myClear();
            toTable();
            buttonOff();
            noCusor();
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png")));
        }
    }
    private void numberKey(KeyEvent evt,JTextField txt){
       char c = evt.getKeyChar(); // Get the typed character 
        if (c != KeyEvent.VK_BACK_SPACE && c != KeyEvent.VK_DELETE) {
            if(!(Character.isDigit(c))){
                if ((c == '.')) {
                    if (txt.getText().indexOf(".") < 0)
                        return;
                    else{ 
                        evt.consume(); //Ignore this key
                        return;
                    } 
                } 
                else{
                    evt.consume(); // Ignore this key 
                    return;
                }
            } 
        }
    }
    private void keyEnter(KeyEvent e,JTextField t){
        if(e.getKeyCode()==KeyEvent.VK_ENTER)
            t.requestFocus();
    }
    private boolean checkValue(){
        boolean b=false;
        if(txtName.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Please input Guest's 'Name'","Missing",JOptionPane.WARNING_MESSAGE);txtName.requestFocus();b=true;
        }else if(txtCard.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Please Input Guest's 'Identify Card'","Missing",JOptionPane.WARNING_MESSAGE);txtCard.requestFocus();b=true;
        }else if(txtContact.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Please Input Guest's 'Contact'","Missing",JOptionPane.WARNING_MESSAGE);txtContact.requestFocus();b=true;
        }else if(txtAddress.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Please Input Guest's 'Address'","Missing",JOptionPane.WARNING_MESSAGE);txtAddress.requestFocus();b=true;
        }else if(cboType.getSelectedIndex()==-1){
            JOptionPane.showMessageDialog(null,"Please Choose 'RoomType'","Missing",JOptionPane.WARNING_MESSAGE);cboType.requestFocus();cboType.showPopup();b=true;
        }else if(cboRName.getSelectedIndex()==-1){
            JOptionPane.showMessageDialog(null,"Please Choose 'Room'","Missing",JOptionPane.WARNING_MESSAGE);cboRName.requestFocus();cboRName.showPopup();b=true;
        }else if(dCheckIn.getDate()==null){
            JOptionPane.showMessageDialog(null,"Please choose 'Date CheckIn'","Missing",JOptionPane.WARNING_MESSAGE);b=true;
        }else if(dCheckOut.getDate()==null){
            JOptionPane.showMessageDialog(null,"Please choose 'Date CheckOut'","Missing",JOptionPane.WARNING_MESSAGE);b=true;
        }else b=false;
        return b;
    }
   
    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        MainForm.main(null);
        this.dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        formLoad();
    }//GEN-LAST:event_formWindowOpened

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
            // TODO add your handling code here:
        tableClick();
    }//GEN-LAST:event_tableMouseClicked

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        // TODO add your handling code here:
        getNew();
    }//GEN-LAST:event_btnNewActionPerformed
   
    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
        try{
            if(checkValue()==false){
                String getCheckin=((JTextField)dCheckIn.getDateEditor().getUiComponent()).getText();    //convert Jcalendar into Jtextfield that can get its text
                String getCheckOut=((JTextField)dCheckOut.getDateEditor().getUiComponent()).getText();

                SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");    //formate date
                Date din=sdf.parse(getCheckin);     //convert to date to calculat
                Date dout=sdf.parse(getCheckOut);
                sdf.format(din);
                sdf.format(dout);

                if(dout.getTime()<din.getTime()){
                    JOptionPane.showMessageDialog(null,"Date CheckIn must be smaller than Date CheckOut!");
                }else  getUpdate();
            clearText();
            myStatue();
            getRoom();
            myTotal();
            }
        }catch(Exception e){e.printStackTrace();}
    }//GEN-LAST:event_btnSaveActionPerformed
    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        getEdit();
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        // TODO add your handling code here:
        myDelete();
        myClear();
        myTotal();
        noCusor();
        buttonOff();
        btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png")));
   
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void dCheckOutPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_dCheckOutPropertyChange
        if(dCheckIn.getDate()!=null && dCheckOut.getDate()!=null){
            txtPrepaid.setFocusable(true);
            txtPrepaid.requestFocus();
        }else txtPrepaid.setFocusable(false);
    }//GEN-LAST:event_dCheckOutPropertyChange
    public void myPayment(){
        double amount=0,prepaid=0;
        for(int i=0;i<table.getRowCount();i++){
            double a=(Double) model.getValueAt(i,13);
            amount+=a;
            double b=(Double) model.getValueAt(i, 12);
            prepaid+=b;
        }
        txtPaid.setText(prepaid+"");
        txtAmount.setText(amount+"");
        txtGTotal.setText(amount-prepaid+"");
    }
    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        
        txtSearch.setForeground(Color.BLACK);
        while(model.getRowCount()>0){
                        for(int j=0;j<model.getRowCount();j++)
                        model.removeRow(j);
        }
        try{
            String sql="Select *From vguestdetail where (guname Like ? || contact Like ? || idc Like ? || idran Like ?) and rstatue=?";
            
           PreparedStatement pst=ConnectDB.con.prepareStatement(sql);
            pst.setString(1,"%"+txtSearch.getText()+"%");
            pst.setString(2,"%"+txtSearch.getText()+"%");
            pst.setString(3,"%"+txtSearch.getText()+"%");
            pst.setString(4,"%"+txtSearch.getText()+"%");
            pst.setString(5,"Checkin");
            r=pst.executeQuery();
            CGuest gu;
            while(r.next()){
               gu=new CGuest(Integer.parseInt(r.getString("guID")),r.getString("guName"),r.getString("rtType"),r.getString("rName"),
                        Double.parseDouble(r.getString("rtPrice")),r.getString("rtDes"),r.getString("rtSize"),r.getString("checkin"),
                        r.getString("checkout"),r.getString("contact"),r.getString("idc"),r.getString("guAdd"),Integer.parseInt(r.getString("dns")),
                        Double.parseDouble(r.getString("prepaid")),Double.parseDouble(r.getString("amount")),r.getString("idran"));
                  Object row[]={
                gu.getId(),
                gu.getName(),
                gu.getCard(),
                gu.getContact(),
                gu.getAdd(),
                gu.getRName(),//room name
                gu.getRttype(),
                gu.getSize(),
                gu.getPrice(),
                gu.getDes(),//room descrition
                gu.getChin(),
                gu.getChout(),
                gu.getDns(),
                gu.getPrepaid(),
                gu.getAmount(),
                gu.getIdran()
            };
            model.addRow(row);
            }    
            if(evt.getKeyCode()==KeyEvent.VK_ENTER)myTotal();
        }catch(SQLException e){e.printStackTrace();}
        
 
    }//GEN-LAST:event_txtSearchKeyPressed
    
    private void txtSearchFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSearchFocusGained
        // TODO add your handling code here:
        txtSearch.setText("");
    }//GEN-LAST:event_txtSearchFocusGained

    private void txtNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNameKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){            
            txtCard.requestFocus();
        }
    }//GEN-LAST:event_txtNameKeyPressed

    private void txtSearchFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSearchFocusLost
        txtSearch.setForeground(Color.DARK_GRAY);
        txtSearch.setText("Search name,contact,card");
    }//GEN-LAST:event_txtSearchFocusLost

    private void btnPaymentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPaymentActionPerformed
        Payment.main(null);
        bb=1;        
    }//GEN-LAST:event_btnPaymentActionPerformed

    private void txtIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIDActionPerformed

    private void cboRNameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cboRNameMouseClicked
        // TODO add your handling code here:
        //if(cboRName.getSelectedItem().equals(null)) return;
         addRoom();
       
    }//GEN-LAST:event_cboRNameMouseClicked

    private void txtContactKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtContactKeyPressed
         if(evt.getKeyCode()==KeyEvent.VK_ENTER)
             txtAddress.requestFocus();
    }//GEN-LAST:event_txtContactKeyPressed

    private void txtNameKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNameKeyTyped
        char ch=evt.getKeyChar();
        if(Character.isDigit(ch)
            || (ch==KeyEvent.VK_BACK_SLASH)
            || (ch==KeyEvent.VK_DELETE)){
        getToolkit().beep();
        evt.consume();
        }
    }//GEN-LAST:event_txtNameKeyTyped

    private void txtContactKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtContactKeyTyped
        int max=9;
        char ch = evt.getKeyChar();
        int a =txtContact.getText().length();
        if(a > max)
        {
            getToolkit().beep();
            evt.consume();
        }
        if(!(Character.isDigit(ch))
            || (ch==KeyEvent.VK_BACK_SLASH)
            || (ch==KeyEvent.VK_DELETE)
            || (ch==KeyEvent.VK_ENTER)){
        evt.consume();
        }
        
    }//GEN-LAST:event_txtContactKeyTyped

    private void txtCardKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCardKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER)
            txtContact.requestFocus();
    }//GEN-LAST:event_txtCardKeyPressed

    private void txtCardKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCardKeyTyped
         int max=8;
        char ch = evt.getKeyChar();
        int a =txtCard.getText().length();
        if(a > max)
        {
            getToolkit().beep();
            evt.consume();
        }
        if(!(Character.isDigit(ch))
            || (ch==KeyEvent.VK_BACK_SLASH)
            || (ch==KeyEvent.VK_DELETE)
            || (ch==KeyEvent.VK_ENTER)){
        evt.consume();
        }
    }//GEN-LAST:event_txtCardKeyTyped

    private void txtPrepaidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrepaidActionPerformed
        Double d=(Integer.parseInt(txtDay.getText())*Double.parseDouble(txtPrice.getText()));
        txtTotal.setText(d+"");
        btnSave.doClick();
    }//GEN-LAST:event_txtPrepaidActionPerformed

    private void txtPrepaidFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPrepaidFocusGained
        dateCompare();
         try{
            Double d=(Integer.parseInt(txtDay.getText())*Double.parseDouble(txtPrice.getText()));
            txtTotal.setText(d+"");txtPrepaid.requestFocus();
           
        }catch(Exception e){e.printStackTrace();}
    }//GEN-LAST:event_txtPrepaidFocusGained

    private void txtPrepaidKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrepaidKeyTyped
        numberKey(evt, txtPrepaid);
    }//GEN-LAST:event_txtPrepaidKeyTyped

    private void cboTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTypeActionPerformed
        getRoom();
    }//GEN-LAST:event_cboTypeActionPerformed

    private void btnNewMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNewMouseEntered
        btnNew.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnNewMouseEntered

    private void btnSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseEntered
        btnSave.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnSaveMouseEntered

    private void btnEditMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditMouseEntered
        btnEdit.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnEditMouseEntered

    private void btnDeleteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMouseEntered
         btnDelete.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnDeleteMouseEntered

    private void btnNewMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNewMouseExited
        btnNew.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnNewMouseExited

    private void btnSaveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseExited
        btnSave.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnSaveMouseExited

    private void btnEditMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditMouseExited
        btnEdit.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnEditMouseExited

    private void btnDeleteMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMouseExited
        btnDelete.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnDeleteMouseExited

    private void btnPaymentMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPaymentMouseExited
        btnPayment.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnPaymentMouseExited

    private void btnPaymentMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPaymentMouseEntered
        btnPayment.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnPaymentMouseEntered

    private void btnBackMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBackMouseEntered
        btnBack.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnBackMouseEntered

    private void btnBackMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBackMouseExited
        btnBack.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnBackMouseExited

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try {
            Robot robot=new Robot();
                robot.keyPress(KeyEvent.VK_UP);
        } catch (AWTException ex) {
            Logger.getLogger(Guest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_txtSearchKeyReleased
    public void table(String sql){
        try{
        st=ConnectDB.con.createStatement();
        r=st.executeQuery(sql);
        while(r.next()){
             model.addRow(new Object[]{r.getString("guID"),r.getString("guName"),r.getString("idc"),r.getString("contact"),
                        r.getString("guadd"),r.getString("rname"),r.getString("rttype"),
                        r.getString("rtsize"),r.getString("rtprice"),r.getString("rtDes"),r.getString("checkin"),r.getString("checkout"),
                        r.getString("prepaid"),r.getString("amount"),r.getString("idran")
                 });
        }
        }catch(SQLException e){e.printStackTrace();}    
    }
   
    
    public void myTotal(){
        try{
        //calculat amount
        double amount=0;
        double paid=0;
        for(int i=0;i<table.getRowCount();i++){
            String a=table.getValueAt(i, 12).toString();
            paid+=Double.parseDouble(a);
            String b=(table.getValueAt(i,13).toString());
            //JOptionPane.showMessageDialog(null, a);
            amount+=Double.parseDouble(b);
            //calculat paid
         //   double b=((Double) table.getValueAt(i,12));
          //  paid+=b;
        }
        double total=amount-paid;
        txtPaid.setText("$"+paid);
        txtAmount.setText("$"+amount);
        txtGTotal.setText("$"+total);
        }catch(ArithmeticException e){e.printStackTrace();}
    }
    public void myRandom(){
        Random ran=new Random();
        String id=String.valueOf(ran.nextInt(87654321));
        txtRID.setText(id+"");
    }
    public void tableClick(){ 
        try{
            btnEdit.setText("Edit");
            btnSave.setText("Save");
        txtID.setText(model.getValueAt(table.getSelectedRow(), 0).toString());
        txtName.setText(model.getValueAt(table.getSelectedRow(),1).toString());
        txtCard.setText(model.getValueAt(table.getSelectedRow(),2).toString());
        txtContact.setText(model.getValueAt(table.getSelectedRow(),3).toString());
        txtAddress.setText(model.getValueAt(table.getSelectedRow(),4).toString());
        cboType.setSelectedItem(""+model.getValueAt(table.getSelectedRow(), 6));
        //cboRName.setSelectedItem(""+model.getValueAt(table.getSelectedRow(), 5));
        //JOptionPane.showMessageDialog(null,""+model.getValueAt(table.getSelectedRow(),5));
        txtSize.setText(model.getValueAt(table.getSelectedRow(),7).toString());
        txtPrice.setText(model.getValueAt(table.getSelectedRow(), 8).toString());
        txtFloor.setText(model.getValueAt(table.getSelectedRow(),9).toString());
        Date d1=new SimpleDateFormat("yyyy-MM-dd").parse((String)model.getValueAt(table.getSelectedRow(),10).toString());
        dCheckIn.setDate(d1);
        Date d2=new SimpleDateFormat("yyyy-MM-dd").parse((String)model.getValueAt(table.getSelectedRow(), 11).toString());
        dCheckOut.setDate(d2);
        txtDay.setText(model.getValueAt(table.getSelectedRow(),12).toString());
        txtPrepaid.setText(model.getValueAt(table.getSelectedRow(),13).toString());
        //txtDay.setText(model.getValueAt(table.getSelectedRow(), 9).toString());       
        txtTotal.setText(model.getValueAt(table.getSelectedRow(),14).toString());
        txtRID.setText(model.getValueAt(table.getSelectedRow(),15).toString());
        
        buttonOff();
        btnDelete.setEnabled(true);
        btnEdit.setEnabled(true);
        btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png"))); 
        noCusor();
        }catch(Exception e){e.printStackTrace();}
    }
   
    public void myUpdate(){
        String pre=txtPrepaid.getText();
        String rid=getRID();
        double pr=0;
        if(pre.trim().equals("")){
            pr=0.0;
        }else pr=Double.parseDouble(txtPrepaid.getText());
         String getCheckin=((JTextField)dCheckIn.getDateEditor().getUiComponent()).getText();    //convert Jcalendar into Jtextfield that can get its text
        String getCheckOut=((JTextField)dCheckOut.getDateEditor().getUiComponent()).getText(); 
        double amount=Double.parseDouble(txtTotal.getText())-pr;
        try{
             String sql="UPDATE tblGuest SET guName = '"+txtName.getText()+"',idc='"+Integer.parseInt(txtCard.getText())+"',guAdd='"+
                        txtAddress.getText()+"',rid='"+rid+"',rtID='"+getrtID()+"',roPrice='"+Double.parseDouble(txtPrice.getText())
                        +"',roSize='"+txtSize.getText()+"',checkIn='"+getCheckin+"',checkOut='"+
                        getCheckOut+"',contact='"+txtContact.getText()+"',dns='"+Integer.parseInt(txtDay.getText())+"',prepaid='"+
                        pr+"',amount='"+amount+"' WHERE guID="+
                        Integer.parseInt(txtID.getText());  
                st=ConnectDB.con.createStatement();
                st.executeUpdate(sql);
            JOptionPane.showMessageDialog(null,"Update Successful");
            myTotal();
        }catch(SQLException e){e.printStackTrace();}
    }
    public void myDelete(){
        int ok=JOptionPane.showConfirmDialog(this,"Are you sure want to delele!","Confirm",JOptionPane.YES_NO_OPTION);
        if(ok==JOptionPane.OK_OPTION)
        try{
            st=ConnectDB.con.createStatement();
            st.execute("Delete from tblGuest where idran='"+txtRID.getText()+"' and guid='"+txtID.getText()+"'");
            st.close();
            myClear();
            JOptionPane.showMessageDialog(null,"Delete succesfully!");
            myTotal();
            int row=table.getSelectedRow();
            model.removeRow(row);
        }catch(SQLException e){e.printStackTrace();}
    }
    public void checkDate(){
        try{
            Date cd=new Date();
            String d=new SimpleDateFormat("dd").format(cd);
            String m=new SimpleDateFormat("MM").format(cd);
            String y=new SimpleDateFormat("yyyy").format(cd);
        }catch(Exception e){e.printStackTrace();}
      
    }
    public void getCusor(){
        txtName.setFocusable(true);
        cboType.setFocusable(true);
        txtAddress.setFocusable(true);
        txtCard.setFocusable(true);
      //  txtSize.setFocusable(true);
       // txtPrice.setFocusable(true);
        dCheckIn.setFocusable(true);
        dCheckOut.setFocusable(true);
        txtContact.setFocusable(true);
      //  txtDay.setFocusable(true);
      //  txtPrepaid.setFocusable(true);
       // txtAmount.setFocusable(true);
        txtName.requestFocus();
    }
    public void noCusor(){
        
        txtName.setFocusable(false);
        cboType.setFocusable(false);
       // txtSize.setFocusable(false);
        txtPrice.setFocusable(false);
        txtAddress.setFocusable(false);
        txtCard.setFocusable(false);
     //   dCheckIn.setFocusable(false);
     //   dCheckOut.setFocusable(false);
        txtContact.setFocusable(false);
        txtDay.setFocusable(false);
        txtPrepaid.setFocusable(false);
        txtTotal.setFocusable(false);

    }
    public void dateCompare(){
        try{
            String getCheckin=((JTextField)dCheckIn.getDateEditor().getUiComponent()).getText();    //convert Jcalendar into Jtextfield that can get its text
            String getCheckOut=((JTextField)dCheckOut.getDateEditor().getUiComponent()).getText();  
           
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");    //formate date
            Date din=sdf.parse(getCheckin);     //convert to date to calculat
            Date dout=sdf.parse(getCheckOut);
            sdf.format(din);
            sdf.format(dout);
            long in=0, day,num;
           
//            if(dout.getTime()<din.getTime()){
//                JOptionPane.showMessageDialog(null,"Date CheckIn must be smaller than Date CheckOut!");
//                txtPrepaid.requestFocus();return;
//               // txtPrepaidFocusGained(evt); return;
            //}else 
            if(dout.getTime()>din.getTime()){
                in=dout.getTime()-din.getTime();
                long a=(long)((long)din.getTime()/(1000.0*60*60*24));  //calculat date
                long b=(long)((long)dout.getTime()/(1000.0*60*60*24));
                day=(long)((long)in/(1000.0*60*60*24));
                txtDay.setText(day+"");txtPrepaid.requestFocus();
             }else if(dout.getTime()==din.getTime()){
                 txtDay.setText("1");
             }else if(dout.getTime()<din.getTime()){
                 txtDay.setText("-00");
             }
            
             /*
            do{
            if(din.after(dout)){  //compare din is bigger than dout
                JOptionPane.showMessageDialog(null,"Date CheckIn must be smaller than Date CheckOut!");
                txtPrepaid.requestFocus();break;
            }else if(din.equals(dout)){   //compare checkin equals to checkout
                txtDay.setText("1");
                txtPrepaid.requestFocus();
            }else if(din.before(dout)){   //compare checkout is bigger than checkin
                in=dout.getTime()-din.getTime();
                day=(long)((long)in/(1000.0*60*60*24));//find staying day;
                txtDay.setText(""+day);
                txtPrepaid.requestFocus();
            }else if(dout.after(din)){
                JOptionPane.showMessageDialog(null,"Date checkin must be smaller than Date CheckOut");
                txtPrepaid.requestFocus();
            }
            }while(btnSave.getText()==setSave);
*/
          txtPrepaid.requestFocus();
        }catch(ParseException e){e.printStackTrace();}
    }
    public void addRoom(){
           try{
            String cbo=cboType.getSelectedItem().toString();
         
            String sql="Select rtSize,rtPrice,rtDes from tblroomtype where rttype='"+cbo+"'";
            st=ConnectDB.con.createStatement();
            r=st.executeQuery(sql);
         //   if(cbo==null){txtSize.setText("0");txtPrice.setText("0");
            if(r.first()){
               // txtName.setText(r.getString("rtName"));
                txtSize.setText(r.getString("rtSize"));
                txtPrice.setText(r.getString("rtPrice"));
                txtFloor.setText(r.getString("rtDes"));
            }
           // r.close();
           // st.close();
        }catch(SQLException e){e.printStackTrace();}
    }
    public ArrayList<String> getCombo(){        
        ArrayList<String>arr=new ArrayList<String>();
        String sql="Select Distinct rtType from tblroomtype order by rtType";
        try{
            st=ConnectDB.con.createStatement();
            r=st.executeQuery(sql);
            if(r.first())
            do{                
                arr.add(r.getString("rtType"));
            }while(r.next());
             r.close();
            st.close();
        }catch(SQLException e){e.printStackTrace();}    
        return arr;
    }
    public void addCombo(){
        ArrayList<String>arr=getCombo();
        for(int i=0;i<arr.size();i++){
            String a=arr.get(i);
            cboType.addItem(a);
        }
    }
    public void changeStatue(){
        String sql= "Update tblroom set rstatue='Book' where rname='"+cboRName.getSelectedItem().toString()+"'";
        try{
        st=ConnectDB.con.createStatement();
        st.executeUpdate(sql);
        //getRoom();
        }catch(SQLException e){e.printStackTrace();}
    }
    public void mySave(){
        String rtID=getrtID();
        String rid=getRID();
        String pre=txtPrepaid.getText();
        double pr=0;
        if(pre.trim().equals("")){
            pr=0.0;
        }else {pr=Double.parseDouble(txtPrepaid.getText());
        }
        int idc=Integer.parseInt(txtCard.getText());
        String addr=txtAddress.getText();
        double rtPrice=Double.parseDouble(txtPrice.getText());
        String roSize=txtSize.getText();
       // String rid=cboRName.getSelectedItem().toString();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        String n=txtName.getText().trim();
       // String idca=txtCard.getText().trim();
        String con=txtContact.getText().trim();
        //String add=txtAddress.getText().trim();
        String rt=cboType.getSelectedItem().toString();
        String rn=cboRName.getSelectedItem().toString();
       // String size=txtSize.getText();
       // String price=txtPrice.getText();
        String des=txtFloor.getText();
       
       // String prep=txtPrepaid.getText();
        String total=txtTotal.getText();
        String ran=txtRID.getText();
        double amount=Double.parseDouble(total)-pr;
        String getCheckin=((JTextField)dCheckIn.getDateEditor().getUiComponent()).getText();    //convert Jcalendar into Jtextfield that can get its text
        String getCheckOut=((JTextField)dCheckOut.getDateEditor().getUiComponent()).getText();  
        String sql="Insert into tblguest (guName,idc,guAdd,rID,rtID,roPrice,roSize,checkIn,checkOut,contact,dns,prepaid,amount,idran) values('"+
                        n+"','"+idc+"','"+addr+"','"+rid+"','"+rtID+"','"+rtPrice+"','"+roSize+"','"+getCheckin+"','"+getCheckOut+"','"+
                        con+"','"+Integer.parseInt(txtDay.getText())+"','"+
                        pr+"','"+amount+"','"+ran+"')";
        try{
            st=ConnectDB.con.createStatement();
            st.execute(sql);
            st.close();
            JOptionPane.showMessageDialog(null,"Save Successfully!");
            myTotal();
          //  model.addRow(new Object[]{"",n,idc,con,addr,rn,rt,roSize,rtPrice,des,getCheckin,getCheckOut,pr,total,ran});
            clearText();
        }catch(SQLException e){e.printStackTrace();}
    }
    public void getRoom(){
        String sql="select rname from vroomtypedetail where rttype='"+cboType.getSelectedItem().toString()+"'  && rStatue='Free' order by rname";
        try{
            st=ConnectDB.con.createStatement();
            r=st.executeQuery(sql);
            cboRName.removeAllItems();
            while(r.next()){
                cboRName.addItem(r.getString("rname"));
            }
//            r.close();
//            st.close();
        }catch(SQLException e){e.printStackTrace();}
    }
    public String getRID(){
        String id="";
        String sql="Select rid from tblroom where rname='"+cboRName.getSelectedItem().toString()+"'";
        try{
            st=ConnectDB.con.createStatement();
            r=st.executeQuery(sql);
            while(r.next()){
                id=r.getString("rid");
            }
            r.close();
            st.close();
        }catch(SQLException e){e.printStackTrace();}
        return id;
    }
    public String  getrtID(){
        String id="";
        try{
            String sql="Select rtID from tblRoomType where rtType='"+cboType.getSelectedItem().toString()+"'";
            st=ConnectDB.con.createStatement();
            r=st.executeQuery(sql);
            while(r.next()){
                   id=r.getString("rtID");
            }
            r.close();
            st.close();
        }catch(SQLException e){e.printStackTrace();}
        return id;
    }
   
    public void initTable(){
        model.addColumn("G_ID");
        model.addColumn("Name");
        //model.addColumn("rtType");
        model.addColumn("Identify Card");
        model.addColumn("Contact");
        model.addColumn("Address");
        model.addColumn("Room Name");
        model.addColumn("Room Type");
        model.addColumn("Size");
        model.addColumn("Price");
        model.addColumn("Floor");
        model.addColumn("CheckIn");
        model.addColumn("CheckOut");
        model.addColumn("Day");
        model.addColumn("Prepaid");
        model.addColumn("Total");
        model.addColumn("Invoice_Num");
        table.setModel(model);
      //  table.getColumnModel().getColumn(0).setPreferredWidth(40);
        table.getColumnModel().getColumn(1).setPreferredWidth(130);
        table.getColumnModel().getColumn(2).setPreferredWidth(100);
        table.getColumnModel().getColumn(3).setPreferredWidth(100);
        table.getColumnModel().getColumn(4).setPreferredWidth(100);
        table.getColumnModel().getColumn(5).setPreferredWidth(100);
        table.getColumnModel().getColumn(6).setPreferredWidth(100);
        table.getColumnModel().getColumn(7).setPreferredWidth(70);
        table.getColumnModel().getColumn(8).setPreferredWidth(80);
        table.getColumnModel().getColumn(9).setPreferredWidth(80);
        table.getColumnModel().getColumn(10).setPreferredWidth(100);
        table.getColumnModel().getColumn(11).setPreferredWidth(100);
        table.getColumnModel().getColumn(12).setPreferredWidth(50);
        table.getColumnModel().getColumn(13).setPreferredWidth(80);
        table.getColumnModel().getColumn(14).setPreferredWidth(80);
        //table.getColumnModel().getColumn(15).setPreferredWidth(100);
        table.removeColumn(table.getColumnModel().getColumn(15));
        table.removeColumn(table.getColumnModel().getColumn(0));
        
    }
    public void toTable(){
        getDB();
    }
    public void getDB(){
        try{
            while(model.getRowCount()>0){
                        for(int j=0;j<model.getRowCount();j++)
                        model.removeRow(j);
                        }
            String sql="Select *from vguestdetail where idran='"+txtRID.getText()+"' ";
            st=ConnectDB.con.createStatement();
            r=st.executeQuery(sql);
            CGuest gu;
            while(r.next()){
                 gu=new CGuest(Integer.parseInt(r.getString("guID")),r.getString("guName"),r.getString("rtType"),r.getString("rName"),
                        Double.parseDouble(r.getString("rtPrice")),r.getString("rtDes"),r.getString("rtSize"),r.getString("checkin"),
                        r.getString("checkout"),r.getString("contact"),r.getString("idc"),r.getString("guAdd"),Integer.parseInt(r.getString("dns")),
                        Double.parseDouble(r.getString("prepaid")),Double.parseDouble(r.getString("amount")),r.getString("idran"));
               Object row[]={
                gu.getId(),
                gu.getName(),
                gu.getCard(),
                gu.getContact(),
                gu.getAdd(),
                gu.getRName(),//room name
                gu.getRttype(),
                gu.getSize(),
                gu.getPrice(),
                gu.getDes(),//room descrition
                gu.getChin(),
                gu.getChout(),
                gu.getDns(),
                gu.getPrepaid(),
                gu.getAmount(),
                gu.getIdran()
            };
            model.addRow(row);
            }
        }catch(SQLException e){e.printStackTrace();}
    }
        
    public void myBackup(){
       
            Date cd=new Date();
            String d=new SimpleDateFormat("dd").format(cd);
            String m=new SimpleDateFormat("MM").format(cd);
            String y=new SimpleDateFormat("yyyy").format(cd);       
        String sql="Insert into tblcheckout select *from tblguest where checkout< '"+y+"-"+m+"-"+d+"'";
        try{
            st=ConnectDB.con.createStatement();
                st.execute(sql);
            st.execute("Delete from tblguest where checkout<'"+y+"-"+m+"-"+d+"'");
            
        }catch(SQLException e){e.printStackTrace();}
        
    }
    public void myStatue(){
          Date cd=new Date();
            String d=new SimpleDateFormat("dd").format(cd);
            String m=new SimpleDateFormat("MM").format(cd);
            String y=new SimpleDateFormat("yyyy").format(cd);   
        String sql3="Update tblroom set rstatue='Free'";
        String sql2="Update vroomdetail set rstatue='Book' where checkin>'"+y+"-"+m+"-"+d+"'";
        String sql="Update vroomdetail set rstatue='Free' where checkout<'"+y+"-"+m+"-"+d+"'";
        String sql1="Update vroomdetail set rstatue='Checkin' where checkin<='"+y+"-"+m+"-"+d+"' and checkout>='"+y+"-"+m+"-"+d+"'";
        try{
            st=ConnectDB.con.createStatement();
            st.executeUpdate(sql3);
            st.executeUpdate(sql2);
            st.executeUpdate(sql);
            st.executeUpdate(sql1);
             }catch(SQLException e){e.printStackTrace();}
            
    }
    
    public Vector<CGuest> getDataFromDB(){
        Vector<CGuest>vec=new Vector<CGuest>();
        CGuest gu;
        try{
            String sql="Select *From vguestdetail";
            st=ConnectDB.con.createStatement();
            r=st.executeQuery(sql);
            while(r.next()){
               gu=new CGuest(Integer.parseInt(r.getString("guID")),r.getString("guName"),r.getString("rtType"),r.getString("rName"),
                        Double.parseDouble(r.getString("rtPrice")),r.getString("rtDes"),r.getString("rtSize"),r.getString("checkin"),
                        r.getString("checkout"),r.getString("contact"),r.getString("idc"),r.getString("guAdd"),Integer.parseInt(r.getString("dns")),
                        Double.parseDouble(r.getString("prepaid")),Double.parseDouble(r.getString("amount")),r.getString("idran"));
                 vec.add(gu);
            }
            st.close();
            r.close();
        }catch(SQLException e){e.printStackTrace();}
        return vec;
    }
    public void myClear(){
        txtName.setText("");
        //cboType.setSelectedItem(null);
        cboRName.setSelectedItem(null);
        txtSize.setText("");
        txtPrice.setText("");
        txtFloor.setText("");
        txtAddress.setText("");
        txtCard.setText("");
        dCheckIn.setDate(null);
        dCheckOut.setDate(null);
        txtContact.setText("");
        txtDay.setText("");
        txtPrepaid.setText("");
        txtTotal.setText("");
        txtAmount.setText("");
        txtPaid.setText("");
        txtGTotal.setText("");
        txtName.requestFocus();
    }
    public void clearText(){
        //cboType.setSelectedItem(null);
       // cboRName.setSelectedItem(null);
        dCheckIn.setDate(null);
        dCheckOut.setDate(null);
        txtSize.setText("");
        txtPrice.setText("");
        txtFloor.setText("");
       // txtDay.setText("");
        txtPrepaid.setText("");
        txtDay.setText("");
        txtTotal.setText("");
       // txtGTotal.setText("0");
        
    }
    Runnable runn=new Runnable() {
        @Override
        public void run() {
           // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        try{
        SimpleDateFormat sdf=new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat sdf1=new SimpleDateFormat("d-MMM-yyyy");
        lblDate.setText(sdf1.format(System.currentTimeMillis()));
        while(true){
        lblTime.setText(sdf.format(System.currentTimeMillis()));
        Thread.sleep(1000);
        }
        }catch(Exception e){e.printStackTrace();}
        }
    };
    Thread th;
    public void myDateTime(){
       th=new Thread(runn);
       th.start();
       
    }
    
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Guest().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnPayment;
    private javax.swing.JButton btnSave;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cboRName;
    private javax.swing.JComboBox<String> cboType;
    private com.toedter.calendar.JDateChooser dCheckIn;
    private com.toedter.calendar.JDateChooser dCheckOut;
    private javax.swing.JLabel dd;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblTime;
    public javax.swing.JTable table;
    private javax.swing.JTextArea txtAddress;
    private javax.swing.JTextField txtAmount;
    private jnumberfield.JNumberField txtCard;
    private jnumberfield.JNumberField txtContact;
    private javax.swing.JTextField txtDay;
    private javax.swing.JTextField txtFloor;
    private javax.swing.JTextField txtGTotal;
    private javax.swing.JTextField txtID;
    public javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPaid;
    private javax.swing.JTextField txtPrepaid;
    private javax.swing.JTextField txtPrice;
    private javax.swing.JTextField txtRID;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtSize;
    private javax.swing.JTextField txtTotal;
    // End of variables declaration//GEN-END:variables
    public DefaultTableModel model=new DefaultTableModel();
}
