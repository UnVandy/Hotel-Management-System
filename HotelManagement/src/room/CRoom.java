package room;

public class CRoom {
    private int id;
    private String rname;
    private String rtype;
    private String statue;
    
    public CRoom(){
        this(44,"a001","vip","free");
    }

    public CRoom(int id, String rname, String rtype, String statue) {
        this.id = id;
        this.rname = rname;
        this.rtype = rtype;
        this.statue = statue;
    }

    public int getId() {
        return id;
    }

    public String getRname() {
        return rname;
    }

    public String getRtype() {
        return rtype;
    }

    public String getStatue() {
        return statue;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setRname(String rname) {
        this.rname = rname;
    }

    public void setRtype(String rtype) {
        this.rtype = rtype;
    }

    public void setStatue(String statue) {
        this.statue = statue;
    }

    @Override
    public String toString() {
        return "CRoom{" + "id=" + id + ", rname=" + rname + ", rtype=" + rtype + ", statue=" + statue + '}';
    }
    
}
