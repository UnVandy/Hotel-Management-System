package room;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
public class Room extends javax.swing.JFrame {
    private Statement st;
    private ResultSet r;
    String setNew="New";
    String setCancel="Cancel";
    String setSave="Save";
    String setUpdate="Update";
    String setEdit="Edit";
    public Room() {
        initComponents();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        btnBack = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        txtName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cboType = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        btnDelete = new javax.swing.JButton();
        btnNew = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        txtSearch = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(102, 102, 255));
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(204, 255, 204));
        jPanel1.setForeground(new java.awt.Color(51, 255, 51));

        jLabel1.setFont(new java.awt.Font("Tekton Pro Cond", 3, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 0, 0));
        jLabel1.setText("Room Information");

        txtID.setFocusable(false);
        txtID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIDActionPerformed(evt);
            }
        });

        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/back.png"))); // NOI18N
        btnBack.setBorderPainted(false);
        btnBack.setContentAreaFilled(false);
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jPanel4.setBackground(new java.awt.Color(204, 204, 204));

        txtName.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNameFocusLost(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Room Type:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Room Name:");

        cboType.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        table.setForeground(new java.awt.Color(0, 0, 153));
        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "R_Name", "R_Type", "Statue"
            }
        ));
        table.setGridColor(new java.awt.Color(0, 204, 255));
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(table);

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));

        btnDelete.setBackground(new java.awt.Color(255, 255, 255));
        btnDelete.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/Actions-edit-delete-icon.png"))); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDeleteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnDeleteMouseExited(evt);
            }
        });
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnNew.setBackground(new java.awt.Color(255, 255, 255));
        btnNew.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        btnNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/Folder-Add-icon.png"))); // NOI18N
        btnNew.setText("New");
        btnNew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnNewMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnNewMouseExited(evt);
            }
        });
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/Save-icon.png"))); // NOI18N
        btnSave.setText("Save");
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveMouseExited(evt);
            }
        });
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnEdit.setBackground(new java.awt.Color(255, 255, 255));
        btnEdit.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/Edit-validated-icon.png"))); // NOI18N
        btnEdit.setText("Edit");
        btnEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEditMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnEditMouseExited(evt);
            }
        });
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnNew)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEdit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDelete)
                .addGap(16, 16, 16))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnDelete, btnEdit, btnNew, btnSave});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNew)
                    .addComponent(btnSave)
                    .addComponent(btnDelete)
                    .addComponent(btnEdit))
                .addGap(5, 5, 5))
        );

        txtSearch.setForeground(new java.awt.Color(204, 204, 204));
        txtSearch.setText("Search Name");
        txtSearch.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtSearchFocusGained(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3))
                                .addGap(40, 40, 40)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtName)
                                    .addComponent(cboType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 422, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 343, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(cboType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cboType, txtName});

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(177, 177, 177)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(0, 0, 0)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        addComboBox();
        initTable();
        formLoad();
    }//GEN-LAST:event_formWindowOpened

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try{
            Robot robot=new Robot();
            robot.keyPress(KeyEvent.VK_UP);
        }catch(AWTException e){e.printStackTrace();}

    }//GEN-LAST:event_txtSearchKeyReleased

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        txtSearch.setForeground(Color.BLACK);
        String s=txtSearch.getText()+"%";
        String sql="select rid,rname,rttype,rstatue from vroomtypedetail where rname Like '"+s+"' order by rname";
        myTable(sql);
    }//GEN-LAST:event_txtSearchKeyPressed

    private void txtSearchFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSearchFocusGained
        txtSearch.setText("");
    }//GEN-LAST:event_txtSearchFocusGained

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        getEdit();
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnEditMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditMouseExited
        btnEdit.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnEditMouseExited

    private void btnEditMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditMouseEntered
        btnEdit.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnEditMouseEntered

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        if(checkValue()==false){
            getUpdate();
            initTable();
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnSaveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseExited
        btnSave.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnSaveMouseExited

    private void btnSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseEntered
        btnSave.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnSaveMouseEntered

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed

        getNew();
    }//GEN-LAST:event_btnNewActionPerformed

    private void btnNewMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNewMouseExited
        btnNew.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnNewMouseExited

    private void btnNewMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNewMouseEntered
        btnNew.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnNewMouseEntered

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        myDelete();
        myClear();
        noCusor();
        buttonOff();
        btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png")));
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnDeleteMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMouseExited
        btnDelete.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnDeleteMouseExited

    private void btnDeleteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMouseEntered
        btnDelete.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnDeleteMouseEntered

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
        tableClick();
    }//GEN-LAST:event_tableMouseClicked

    private void txtNameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNameFocusLost

    }//GEN-LAST:event_txtNameFocusLost

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        MainForm.main(null);
        this.dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    private void txtIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIDActionPerformed
    
    private void formLoad(){
        initTable();
        myClear();
        noCusor();       
        txtID.setVisible(false);
        buttonOff();
    }
    private void buttonOff(){
        btnSave.setText("Save");
        btnSave.setEnabled(false);
        btnEdit.setText("Edit");
        btnEdit.setEnabled(false);
        btnDelete.setEnabled(false);
    }
    private void buttonOn(String b1){
        btnSave.setEnabled(true);
        btnEdit.setEnabled(true);
        btnDelete.setEnabled(true);
        btnSave.setText("Save");
        btnEdit.setText("Edit");
    }
   
   
     private void getNew(){
        if(btnNew.getText()=="New"){
           // btnNew.setIcon(new ImageIcon(.getClass().getResource("book/picture/cancel.jpg")));
            btnNew.setIcon(new ImageIcon(getClass().getResource("Picture\\Cancel.png"))); 
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png")));
            getCusor();
            btnNew.setText("Cancel");
            myClear();
            buttonOff();
            btnSave.setEnabled(true);
        }else if(btnNew.getText()=="Cancel"){
            int a=JOptionPane.showConfirmDialog(null,"Do you want to cancel ?","Comfirm",JOptionPane.YES_NO_OPTION);
            if(JOptionPane.YES_OPTION==a){
            btnNew.setIcon(new ImageIcon(getClass().getResource("Picture\\Folder-Add-icon.png")));
            myClear();
            noCusor();
            btnNew.setText("New");
            btnSave.setEnabled(false);
            }
        }
    }
    private void getEdit(){
        if(btnEdit.getText()=="Edit"){
            getCusor();
            btnSave.setText("Update");
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Cancel.png"))); 
            btnEdit.setText("Cancel");
            btnNew.setText("New");
            btnNew.setIcon(new ImageIcon(getClass().getResource("Picture\\Folder-Add-icon.png")));
            btnSave.setEnabled(true);
        }else if(btnEdit.getText()=="Cancel"){
            myClear();
            noCusor();
            btnSave.setText("Save");
            btnEdit.setText("Edit");
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png"))); 
            buttonOff();
        }
    }
    private void getUpdate(){
        if(btnSave.getText()=="Save"){
            mySave();
            myClear();
        }else if(btnSave.getText()=="Update"){
            myUpdate();
            myClear();
            buttonOff();
            noCusor();
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png")));
        }
    }
    private void noCusor(){
        txtName.setFocusable(false);
        cboType.setEnabled(false);
    }
    private void getCusor(){
        txtName.setFocusable(true);
        cboType.setEnabled(true);
    }
    private boolean checkValue(){
        boolean b=false;
        if(txtName.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Please Input room 'Name'","Missing",JOptionPane.WARNING_MESSAGE);txtName.requestFocus();b=true;
        }else if(cboType.getSelectedIndex()==-1){
            JOptionPane.showMessageDialog(null,"Please choose room 'Type'","Missing",JOptionPane.WARNING_MESSAGE); cboType.requestFocus();
                cboType.showPopup();b=true;  
        }else b=false;
        return b;
    }
      
       public void myTable(String sql){
        model=new DefaultTableModel();
        model.addColumn("ID");
        model.addColumn("Room Name");
        model.addColumn("RoomType");
        model.addColumn("Satue");
        table.setModel(model);
        table.getColumnModel().getColumn(1).setPreferredWidth(100);
        table.getColumnModel().getColumn(2).setPreferredWidth(100);
        table.getColumnModel().getColumn(3).setPreferredWidth(70);
        table.removeColumn(table.getColumnModel().getColumn(0));
         try{
            st=ConnectDB.con.createStatement();
            r=st.executeQuery(sql);
            while(r.next()){
               model.addRow(new Object[]{r.getString("rID"),r.getString("rName"),r.getString("rttype"),r.getString("rStatue")});
            }
         }catch(SQLException e){e.printStackTrace();}
    }   
    public void myUpdate(){
        try{
            st=ConnectDB.con.createStatement();
            st.executeUpdate("Update tblRoom set rName='"+txtName.getText()+"',rtID='"+getrtID()+"' Where rID='"+txtID.getText()+"'");
            st.close();
            JOptionPane.showMessageDialog(null,"Update successfully!");
        }catch(SQLException e){e.printStackTrace();}
    }
    public void myDelete(){
        int ok=JOptionPane.showConfirmDialog(this,"Are you sure want to delele!","Confirm",JOptionPane.YES_NO_OPTION);
        if(ok==JOptionPane.OK_OPTION)
        try{
            st=ConnectDB.con.createStatement();
            st.execute("Delete from tblroom where rid='"+txtID.getText()+"'");
            st.close();
            JOptionPane.showMessageDialog(null,"Delete succesfully!");
            myClear();
            int row=table.getSelectedRow();
            model.removeRow(row);
        }catch(SQLException e){e.printStackTrace();}
    }
    public void tableClick(){
        btnEdit.setEnabled(true);
        btnDelete.setEnabled(true);
        txtName.setFocusable(false);
        cboType.setEnabled(false);
        btnSave.setEnabled(false);
        txtID.setText(model.getValueAt(table.getSelectedRow(),0).toString());
        txtName.setText(model.getValueAt(table.getSelectedRow(),1).toString());
        cboType.setSelectedItem(model.getValueAt(table.getSelectedRow(),2).toString());
//        int index=table.getSelectedRow();
//        String a=(String)table.getValueAt(index,2);
//        cboType.setSelectedItem(a);
        btnNew.setIcon(new ImageIcon(getClass().getResource("Picture\\Cancel.png"))); 
        btnNew.setText("Cancel");
        btnEdit.setText("Edit");
        btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png"))); 
            
    }
   
    public void myClear(){
        txtName.setText("");
        cboType.setSelectedItem(null);
        txtName.requestFocus();
    }
    public void mySave(){
        String getrtID=getrtID();       
         try{
            st=ConnectDB.con.createStatement();
            st.execute("insert into tblroom (rname,rstatue,rtid)values('"+txtName.getText()+"','Free','"+getrtID+"')");   
            st.close();
            JOptionPane.showMessageDialog(null,"Data saved to File");
        }catch(SQLException e){e.printStackTrace();}
    }
    public void initTable(){
       String sql="select rid,rname,rttype,rstatue from vroomtypedetail order by rname";
       myTable(sql);
    }
    public void addComboBox(){
       try{
           String sql="Select rtType from tblroomtype";
           st=ConnectDB.con.createStatement();
           r=st.executeQuery(sql);
           while(r.next()){
               cboType.addItem(r.getString("rtType"));
           }
           r.close();
           st.close();
       }catch(SQLException e){e.printStackTrace();}
    }
    public String  getrtID(){
        String id="";
        try{
            String sql="Select rtID from tblRoomType where rtType='"+cboType.getSelectedItem().toString()+"'";
            st=ConnectDB.con.createStatement();
            r=st.executeQuery(sql);
            while(r.next()){
                   id=r.getString("rtID");
            }
            r.close();
            st.close();
        }catch(SQLException e){e.printStackTrace();}
        return id;
    }
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Room().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox<String> cboType;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable table;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
    private DefaultTableModel model;
}

