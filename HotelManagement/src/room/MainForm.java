package room;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import java.sql.*;
import javax.swing.*;
import javax.swing.Timer;
public class MainForm extends javax.swing.JFrame {
    private Login log=new Login();
    private ResultSet rs;
    private Statement st;
    private Timer ti;
    private int x1=1200;
    class RunningLetter extends Thread{
        private int x;
        private int y;
        private JLabel l;
        private int s=5;
        public int getX(){
            return x-s;
        }
        public RunningLetter(){
            x=0; y=1;
        }
        public RunningLetter(JLabel l1,int x,int y){
            l=l1;
            this.x=x; this.y=y;
            
            
        }
        public void run(){
            while(true){
                //x=-250; 
                x=x1-90;
                txtTest.setText("Welcome to Luckii Hotel");
                txtTest.setLocation(getX(), 655);
                s=s+5;
                if(s==x1+435) s=100;
                try{
                    sleep(40);
                }catch(InterruptedException ex){
                    ex.printStackTrace();
                }
            }
        }
        
    }
    public MainForm() {
        initComponents();
        ConnectDB.myConnection();
        this.setSize(1200,700);
        setLocationRelativeTo(null);
        buttonColor();
        getNameL();
       // checkPosition();
        checkC();
        ShowDT();
        //RunningLetter rl=new RunningLetter(txtTest,-100,620);
        RunningLetter rl=new RunningLetter(txtTest,x1+60,20);
        rl.start();
        position.setVisible(false);
        btnCreatOrEditUsers.setVisible(true);
        getPosition();
        cc=0;
    }
    public String getNameLogin(){
        return log.getNameValue();
    }
    public void checkC(){
        if(!position.getText().equalsIgnoreCase("Admin")){
            btnCreatOrEditUsers.setVisible(false);
        }
    }
    void ShowDT(){
        ActionListener Li=new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
               SimpleDateFormat d=new SimpleDateFormat("EEEE,dd/MMMM/yyyy"); 
               SimpleDateFormat t=new SimpleDateFormat("hh:mm:ss a");
               time.setText(t.format(new java.util.Date()));
               date.setText(d.format(new java.util.Date()));
            }
        };
        ti=new Timer(1000,Li);
        ti.start();
    }
    
    public void getNameL(){
       String a;
       a=log.forName1();
       name.setText(a.toUpperCase());
       
    }
    public void checkPosition(){
        boolean help=false;
        String n=log.forName1();
        String na="";
        String d="";
        String sql="SELECT * FROM tbluser";
        try {
            st=ConnectDB.con.createStatement();
            rs=st.executeQuery(sql);
            while(rs.next()){
                na=rs.getString("name");
                if(n.equalsIgnoreCase(na)){
                    d=rs.getString("des");
                    help=true;
                }
            }
            if(help){
                position.setText(d);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    public void buttonColor(){
        btnType.setBackground(new Color(240,240,240));
        btnRoom.setBackground(new Color(240,240,240));
        btnStaff.setBackground(new Color(240,240,240));
        btnCheckIn.setBackground(new Color(240,240,240));
        btSignOut.setBackground(new Color(240,240,240));
        btnCreatOrEditUsers.setBackground(new Color(240,240,240));
        btnCustomer.setBackground(new Color(240,240,240));
        btnCheckOut.setBackground(new Color(240,240,240));
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        position = new javax.swing.JLabel();
        name = new javax.swing.JLabel();
        btnRoom = new javax.swing.JButton();
        btnType = new javax.swing.JButton();
        time = new javax.swing.JLabel();
        date = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txtTest = new javax.swing.JLabel();
        btnStaff = new javax.swing.JButton();
        btnCustomer = new javax.swing.JButton();
        btnCheckIn = new javax.swing.JButton();
        btSignOut = new javax.swing.JButton();
        btnCheckOut = new javax.swing.JButton();
        btnBooking = new javax.swing.JButton();
        btnPayment = new javax.swing.JButton();
        btnCreatOrEditUsers = new javax.swing.JButton();
        btnAboutUs = new javax.swing.JButton();
        btnExit = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(153, 153, 153));
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(null);

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Welcome");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(20, 30, 90, 20);

        position.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        position.setForeground(new java.awt.Color(255, 255, 255));
        position.setText("jLabel3");
        getContentPane().add(position);
        position.setBounds(30, 90, 110, 22);

        name.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        name.setForeground(new java.awt.Color(153, 0, 0));
        name.setText("jLabel3");
        getContentPane().add(name);
        name.setBounds(30, 60, 110, 22);

        btnRoom.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnRoom.setText("Room");
        btnRoom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnRoomMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnRoomMouseExited(evt);
            }
        });
        btnRoom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRoomActionPerformed(evt);
            }
        });
        getContentPane().add(btnRoom);
        btnRoom.setBounds(90, 180, 90, 40);

        btnType.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnType.setText("RoomType");
        btnType.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnTypeMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnTypeMouseExited(evt);
            }
        });
        btnType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTypeActionPerformed(evt);
            }
        });
        getContentPane().add(btnType);
        btnType.setBounds(0, 180, 90, 40);

        time.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        time.setForeground(new java.awt.Color(255, 255, 255));
        time.setText("02:20:10 PM");
        getContentPane().add(time);
        time.setBounds(1050, 650, 110, 17);

        date.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        date.setForeground(new java.awt.Color(255, 255, 255));
        date.setText("Tuesday,20/Apirl/2017");
        getContentPane().add(date);
        date.setBounds(1050, 670, 170, 20);

        jPanel1.setBackground(new java.awt.Color(0, 102, 153));
        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setForeground(new java.awt.Color(204, 255, 204));
        getContentPane().add(jPanel1);
        jPanel1.setBounds(1010, 640, 190, 60);

        txtTest.setFont(new java.awt.Font("Trajan Pro", 1, 36)); // NOI18N
        txtTest.setForeground(new java.awt.Color(255, 255, 255));
        txtTest.setText("1");
        getContentPane().add(txtTest);
        txtTest.setBounds(1170, 650, 580, 40);

        btnStaff.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnStaff.setText("Staff");
        btnStaff.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnStaffMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnStaffMouseExited(evt);
            }
        });
        btnStaff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStaffActionPerformed(evt);
            }
        });
        getContentPane().add(btnStaff);
        btnStaff.setBounds(0, 230, 90, 40);

        btnCustomer.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnCustomer.setText("Customer");
        btnCustomer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCustomerMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCustomerMouseExited(evt);
            }
        });
        btnCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomerActionPerformed(evt);
            }
        });
        getContentPane().add(btnCustomer);
        btnCustomer.setBounds(90, 230, 90, 40);

        btnCheckIn.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnCheckIn.setText("CheckIn");
        btnCheckIn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCheckInMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCheckInMouseExited(evt);
            }
        });
        btnCheckIn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckInActionPerformed(evt);
            }
        });
        getContentPane().add(btnCheckIn);
        btnCheckIn.setBounds(0, 280, 90, 40);

        btSignOut.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btSignOut.setText("Sign Out");
        btSignOut.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btSignOutMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btSignOutMouseExited(evt);
            }
        });
        btSignOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSignOutActionPerformed(evt);
            }
        });
        getContentPane().add(btSignOut);
        btSignOut.setBounds(0, 480, 180, 40);

        btnCheckOut.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnCheckOut.setText("CheckOut");
        btnCheckOut.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCheckOutMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCheckOutMouseExited(evt);
            }
        });
        btnCheckOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckOutActionPerformed(evt);
            }
        });
        getContentPane().add(btnCheckOut);
        btnCheckOut.setBounds(90, 280, 90, 40);

        btnBooking.setBackground(new java.awt.Color(255, 255, 255));
        btnBooking.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnBooking.setText("Booking");
        btnBooking.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnBookingMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnBookingMouseExited(evt);
            }
        });
        btnBooking.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBookingActionPerformed(evt);
            }
        });
        getContentPane().add(btnBooking);
        btnBooking.setBounds(0, 330, 90, 40);

        btnPayment.setBackground(new java.awt.Color(255, 255, 255));
        btnPayment.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnPayment.setText("Payment");
        btnPayment.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnPaymentMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnPaymentMouseExited(evt);
            }
        });
        btnPayment.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPaymentActionPerformed(evt);
            }
        });
        getContentPane().add(btnPayment);
        btnPayment.setBounds(90, 330, 90, 40);

        btnCreatOrEditUsers.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btnCreatOrEditUsers.setText("Create or Edit Users");
        btnCreatOrEditUsers.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCreatOrEditUsersMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCreatOrEditUsersMouseExited(evt);
            }
        });
        btnCreatOrEditUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreatOrEditUsersActionPerformed(evt);
            }
        });
        getContentPane().add(btnCreatOrEditUsers);
        btnCreatOrEditUsers.setBounds(0, 430, 180, 40);

        btnAboutUs.setBackground(new java.awt.Color(255, 255, 255));
        btnAboutUs.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnAboutUs.setText("About Us");
        btnAboutUs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAboutUsMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAboutUsMouseExited(evt);
            }
        });
        btnAboutUs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAboutUsActionPerformed(evt);
            }
        });
        getContentPane().add(btnAboutUs);
        btnAboutUs.setBounds(0, 380, 90, 40);

        btnExit.setBackground(new java.awt.Color(255, 255, 255));
        btnExit.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnExit.setText("Exit");
        btnExit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnExitMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnExitMouseExited(evt);
            }
        });
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });
        getContentPane().add(btnExit);
        btnExit.setBounds(90, 380, 90, 40);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Photos/mainForm.jpg"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, -20, 1250, 740);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void getPosition(){
        String sql="{call selectPosition(?)}";
        try{
            PreparedStatement pst=ConnectDB.con.prepareStatement(sql);
            pst.setString(1, name.getText());
            ResultSet rs=pst.executeQuery();
            while(rs.next()){
                String pos=rs.getString("stPos");
                if(pos.equalsIgnoreCase("Manager")){
                    btnCreatOrEditUsers.setEnabled(true);
                }else {
                    btnCreatOrEditUsers.setEnabled(false);
                    btnRoom.setEnabled(false);
                    btnType.setEnabled(false);
                    btnStaff.setEnabled(false);
                }
            }
        }catch(SQLException e){e.printStackTrace();}
    }
    private void btnTypeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnTypeMouseEntered
        btnType.setBackground(Color.ORANGE);
        btnType.setForeground(Color.blue);
    }//GEN-LAST:event_btnTypeMouseEntered

    private void btnTypeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnTypeMouseExited
        btnType.setBackground(new Color(240,240,240));
        btnType.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_btnTypeMouseExited

    private void btnRoomMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRoomMouseEntered
        btnRoom.setBackground(Color.ORANGE);
        btnRoom.setForeground(Color.blue);
    }//GEN-LAST:event_btnRoomMouseEntered

    private void btnRoomMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRoomMouseExited
        btnRoom.setBackground(new Color(240,240,240));
        btnRoom.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_btnRoomMouseExited

    private void btnStaffMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStaffMouseEntered
        btnStaff.setBackground(Color.ORANGE);
        btnStaff.setForeground(Color.blue);
    }//GEN-LAST:event_btnStaffMouseEntered

    private void btnStaffMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnStaffMouseExited
        btnStaff.setBackground(new Color(240,240,240));
        btnStaff.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_btnStaffMouseExited

    private void btnCustomerMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCustomerMouseExited
        btnCustomer.setBackground(new Color(240,240,240));
        btnCustomer.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_btnCustomerMouseExited

    private void btnCustomerMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCustomerMouseEntered
        btnCustomer.setBackground(Color.ORANGE);
        btnCustomer.setForeground(Color.blue);
    }//GEN-LAST:event_btnCustomerMouseEntered

    private void btnCheckInMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCheckInMouseExited
        btnCheckIn.setBackground(new Color(240,240,240));
        btnCheckIn.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_btnCheckInMouseExited

    private void btnCheckInMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCheckInMouseEntered
        btnCheckIn.setBackground(Color.ORANGE);
        btnCheckIn.setForeground(Color.blue);
    }//GEN-LAST:event_btnCheckInMouseEntered

    private void btnCheckOutMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCheckOutMouseEntered
        btnCheckOut.setBackground(Color.ORANGE);
        btnCheckOut.setForeground(Color.blue);
    }//GEN-LAST:event_btnCheckOutMouseEntered

    private void btnCheckOutMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCheckOutMouseExited
        btnCheckOut.setBackground(new Color(240,240,240));
        btnCheckOut.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_btnCheckOutMouseExited

    private void btnCreatOrEditUsersMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCreatOrEditUsersMouseEntered
        btnCreatOrEditUsers.setBackground(Color.ORANGE);
        btnCreatOrEditUsers.setForeground(Color.blue);
    }//GEN-LAST:event_btnCreatOrEditUsersMouseEntered

    private void btnCreatOrEditUsersMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCreatOrEditUsersMouseExited
        btnCreatOrEditUsers.setBackground(new Color(240,240,240));
        btnCreatOrEditUsers.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_btnCreatOrEditUsersMouseExited

    private void btSignOutMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btSignOutMouseEntered
        btSignOut.setBackground(Color.ORANGE);
        btSignOut.setForeground(Color.blue);
    }//GEN-LAST:event_btSignOutMouseEntered

    private void btSignOutMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btSignOutMouseExited
        btSignOut.setBackground(new Color(240,240,240));
        btSignOut.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_btSignOutMouseExited

    private void btSignOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSignOutActionPerformed
        this.dispose();
        Login.main(null);
    }//GEN-LAST:event_btSignOutActionPerformed

    private void btnBookingMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBookingMouseEntered
        btnBooking.setBackground(Color.ORANGE);
        btnBooking.setForeground(Color.blue); 
    }//GEN-LAST:event_btnBookingMouseEntered

    private void btnBookingMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBookingMouseExited
        btnBooking.setBackground(new Color(240,240,240));
        btnBooking.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_btnBookingMouseExited

    private void btnPaymentMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPaymentMouseEntered
        btnPayment.setBackground(Color.ORANGE);
        btnPayment.setForeground(Color.blue);
    }//GEN-LAST:event_btnPaymentMouseEntered

    private void btnPaymentMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPaymentMouseExited
        btnPayment.setBackground(new Color(240,240,240));
        btnPayment.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_btnPaymentMouseExited

    private void btnAboutUsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAboutUsMouseEntered
        btnAboutUs.setBackground(Color.ORANGE);
        btnAboutUs.setForeground(Color.blue);
    }//GEN-LAST:event_btnAboutUsMouseEntered

    private void btnAboutUsMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAboutUsMouseExited
        btnAboutUs.setBackground(new Color(240,240,240));
        btnAboutUs.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_btnAboutUsMouseExited

    private void btnExitMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnExitMouseEntered
        btnExit.setBackground(Color.ORANGE);
        btnExit.setForeground(Color.blue);
    }//GEN-LAST:event_btnExitMouseEntered

    private void btnExitMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnExitMouseExited
        btnExit.setBackground(new Color(240,240,240));
        btnExit.setForeground(new Color(0,0,0));
    }//GEN-LAST:event_btnExitMouseExited

    private void btnTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTypeActionPerformed
        RoomType.main(null);
        this.dispose();
    }//GEN-LAST:event_btnTypeActionPerformed

    private void btnRoomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRoomActionPerformed
         Room.main(null);
         this.dispose();
    }//GEN-LAST:event_btnRoomActionPerformed

    private void btnStaffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStaffActionPerformed
        Staff.main(null);
        this.dispose();
    }//GEN-LAST:event_btnStaffActionPerformed

    private void btnCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCustomerActionPerformed
        Guest.main(null);
        this.dispose();
    }//GEN-LAST:event_btnCustomerActionPerformed

    private void btnCheckInActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckInActionPerformed
        CheckIn.main(null);
        this.dispose();
    }//GEN-LAST:event_btnCheckInActionPerformed

    private void btnCheckOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckOutActionPerformed
        CheckOut.main(null);
        this.dispose();
    }//GEN-LAST:event_btnCheckOutActionPerformed

    private void btnBookingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBookingActionPerformed
        Booking.main(null);
        this.dispose();
    }//GEN-LAST:event_btnBookingActionPerformed
    public static int cc=0;
    private void btnPaymentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPaymentActionPerformed
        Payment.main(null);
        this.dispose();
        cc=1;
    }//GEN-LAST:event_btnPaymentActionPerformed

    private void btnAboutUsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAboutUsActionPerformed
        Test.main(null);
    }//GEN-LAST:event_btnAboutUsActionPerformed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        MainForm.getWindows();
        int a=JOptionPane.showConfirmDialog(null,"Are you sure want to exit?","Comfirm",JOptionPane.YES_NO_OPTION);
            if(JOptionPane.YES_OPTION==a){
                System.exit(0);}
            else return;
    }//GEN-LAST:event_btnExitActionPerformed

    private void btnCreatOrEditUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreatOrEditUsersActionPerformed
        User.main(null);
        this.dispose();
    }//GEN-LAST:event_btnCreatOrEditUsersActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btSignOut;
    private javax.swing.JButton btnAboutUs;
    private javax.swing.JButton btnBooking;
    private javax.swing.JButton btnCheckIn;
    private javax.swing.JButton btnCheckOut;
    private javax.swing.JButton btnCreatOrEditUsers;
    private javax.swing.JButton btnCustomer;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnPayment;
    private javax.swing.JButton btnRoom;
    private javax.swing.JButton btnStaff;
    private javax.swing.JButton btnType;
    private javax.swing.JLabel date;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    public javax.swing.JLabel name;
    private javax.swing.JLabel position;
    private javax.swing.JLabel time;
    private javax.swing.JLabel txtTest;
    // End of variables declaration//GEN-END:variables
}
