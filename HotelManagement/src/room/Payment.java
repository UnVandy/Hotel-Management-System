package room;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

public class Payment extends javax.swing.JFrame {
    private PreparedStatement pst;
    private ResultSet r;
    public Guest g=new Guest();
    private DecimalFormat df=new DecimalFormat("###,###.##");
    MainForm mm=new MainForm();
    public Payment() {
        initComponents();
        setLocationRelativeTo(null);
        table();
        //addCombo();
        addComboname();
        //comboName();
        getClose();
     //   setCombo();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        btnPrint = new javax.swing.JButton();
        lblNum5 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        cboName = new javax.swing.JComboBox<>();
        lblNum7 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblInvoice2 = new javax.swing.JLabel();
        lblPhone = new javax.swing.JLabel();
        txtPrepaid = new javax.swing.JTextField();
        txtAmount = new javax.swing.JTextField();
        txtTotal = new javax.swing.JTextField();
        lblNum1 = new javax.swing.JLabel();
        lblNum3 = new javax.swing.JLabel();
        lblNum2 = new javax.swing.JLabel();
        txtTotalR = new javax.swing.JTextField();
        lblNum9 = new javax.swing.JLabel();
        txtReceivce = new javax.swing.JTextField();
        lblNum6 = new javax.swing.JLabel();
        txtReturn = new javax.swing.JTextField();
        lblNum10 = new javax.swing.JLabel();
        txtReturnR = new javax.swing.JTextField();
        lblNum12 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        btnBack = new javax.swing.JButton();
        btnClose = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));

        btnPrint.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnPrint.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/2-Hot-Printer-icon.png"))); // NOI18N
        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        lblNum5.setFont(new java.awt.Font("Tekton Pro Ext", 0, 36)); // NOI18N
        lblNum5.setForeground(new java.awt.Color(0, 153, 51));
        lblNum5.setText("Payment");

        jPanel2.setBackground(new java.awt.Color(153, 153, 153));

        cboName.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cboName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboNameActionPerformed(evt);
            }
        });

        lblNum7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNum7.setText("Customer Name:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Phone Number: ");

        lblInvoice2.setBackground(new java.awt.Color(255, 0, 0));
        lblInvoice2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblInvoice2.setForeground(new java.awt.Color(204, 0, 51));
        lblInvoice2.setText("...");

        lblPhone.setBackground(new java.awt.Color(255, 0, 0));
        lblPhone.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblPhone.setForeground(new java.awt.Color(204, 0, 51));
        lblPhone.setText("...");

        txtPrepaid.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtPrepaid.setFocusable(false);

        txtAmount.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtAmount.setFocusable(false);
        txtAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAmountActionPerformed(evt);
            }
        });

        txtTotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtTotal.setFocusable(false);

        lblNum1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNum1.setText("Amount:");

        lblNum3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNum3.setText("G-Total:");

        lblNum2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNum2.setText("Prepaid :");

        txtTotalR.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtTotalR.setFocusable(false);

        lblNum9.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNum9.setText("G-Total:");

        txtReceivce.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtReceivce.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtReceivceFocusGained(evt);
            }
        });
        txtReceivce.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtReceivceActionPerformed(evt);
            }
        });
        txtReceivce.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtReceivceKeyTyped(evt);
            }
        });

        lblNum6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNum6.setText("Receivce:");

        txtReturn.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtReturn.setFocusable(false);

        lblNum10.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNum10.setText("Exchange:");

        txtReturnR.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtReturnR.setFocusable(false);

        lblNum12.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNum12.setText("Exchange:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Invoice:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("$");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("$");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("$");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setText("$");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel8.setText("$");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel9.setText("៛");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel10.setText("៛");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(86, 86, 86)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblNum3)
                            .addComponent(lblNum9)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(lblNum1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblNum2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel4)
                                .addComponent(jLabel5)
                                .addComponent(jLabel6))
                            .addComponent(jLabel9))
                        .addGap(2, 2, 2)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTotalR, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPrepaid, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(lblNum7))
                        .addGap(34, 34, 34)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cboName, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblPhone))))
                .addGap(43, 43, 43)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblNum10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel8))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lblNum12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel10)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtReturnR)
                            .addComponent(txtReturn, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(lblNum6)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(42, 42, 42)))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblInvoice2)
                            .addComponent(txtReceivce, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtAmount, txtPrepaid, txtReceivce, txtReturn});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {lblNum12, lblNum9});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNum7)
                    .addComponent(cboName, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(lblInvoice2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lblPhone))
                .addGap(27, 27, 27)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtPrepaid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblNum2)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblNum1)
                            .addComponent(txtAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblNum3)
                            .addComponent(jLabel6)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtReceivce, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblNum6)
                            .addComponent(jLabel7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtReturn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblNum10)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtReturnR, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblNum12)
                            .addComponent(jLabel10))))
                .addGap(0, 0, 0)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNum9)
                    .addComponent(txtTotalR, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {lblNum12, lblNum9});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {lblNum1, lblNum10, lblNum2, lblNum6});

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtAmount, txtPrepaid, txtReceivce, txtReturn});

        jScrollPane2.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane2.setViewportView(table);

        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/back.png"))); // NOI18N
        btnBack.setBorderPainted(false);
        btnBack.setContentAreaFilled(false);
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnClose.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/Delete.png"))); // NOI18N
        btnClose.setContentAreaFilled(false);
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(214, 214, 214)
                .addComponent(lblNum5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnClose))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNum5)
                    .addComponent(btnClose))
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPrint, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
   
    private void setCombo(){
        Guest g=new Guest();
        txtReceivce.setText(g.txtName.getText());
    }
    public void getClose(){
        if(g.bb==1){btnBack.setVisible(false);btnClose.setVisible(true);}
        if(mm.cc==1){btnBack.setVisible(true);btnClose.setVisible(false);}
    }
    private void cboNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboNameActionPerformed
        searchComboName();
        txtReceivce.setText("");
        txtReturn.setText("");       
        txtReturnR.setText("");
        txtReceivce.requestFocus();
    }//GEN-LAST:event_cboNameActionPerformed
    private void myReceivce(){
        
        if(txtReceivce.getText().equals("")){
            txtReceivce.setText("");
            txtReturn.setText("");
        }else{
        double re=Double.parseDouble(txtReceivce.getText());
        double total=Double.parseDouble(txtTotal.getText());
        if(re<total)
            JOptionPane.showMessageDialog(null,"Receivec customer's money is lower than total");
        else {
            txtReturn.setText(re-total+"");
            txtReturnR.setText(df.format((re-total)*4000)+"");
        }
        }
    }
    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        myReceivce();
        SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
        String date=systemDate();
        MainForm m=new MainForm();
        try{
            //Date d1=sdf.format(table.getValueAt(0,7).toString());
            //Date d2=sdf.parse(table.getValueAt(0,8).toString());
            JasperReport jr=JasperCompileManager.compileReport("Report\\hotel5.jrxml");//for complier report
            HashMap para=new HashMap();//for input parameter to ireport
            para.put("cashier",m.name.getText());
            para.put("customer",table.getValueAt(0,0));
            para.put("checkin",table.getValueAt(0,7));
            para.put("checkout",table.getValueAt(0,8));
            para.put("invoice",lblInvoice2.getText());
            para.put("date",date);
            para.put("received",txtReceivce.getText());
            para.put("exchange",txtReturn.getText());
            para.put("subtotal","$"+txtAmount.getText());
            para.put("prepaid","$"+txtPrepaid.getText());
            para.put("grandtotal","$"+txtTotal.getText());
            para.put("totalrail",txtTotalR.getText());
            //para.put("date",);
            JRBeanCollectionDataSource ds=new JRBeanCollectionDataSource(data());//for add field(record)
            JasperPrint print=JasperFillManager.fillReport(jr, para, ds); //build and connected to printer
            JasperViewer.viewReport(print,false); //for view report
        }catch(Exception e){e.printStackTrace();}

    }//GEN-LAST:event_btnPrintActionPerformed

    private void txtAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAmountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAmountActionPerformed

    private void txtReceivceFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtReceivceFocusGained
       
    }//GEN-LAST:event_txtReceivceFocusGained

    private void txtReceivceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtReceivceActionPerformed
        myReceivce();
    }//GEN-LAST:event_txtReceivceActionPerformed

    private void txtReceivceKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtReceivceKeyTyped
        numberKey(evt, txtReceivce);
    }//GEN-LAST:event_txtReceivceKeyTyped

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
       MainForm.main(null);
       this.dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        this.dispose(); 
    }//GEN-LAST:event_btnCloseActionPerformed
    private void numberKey(KeyEvent evt,JTextField txt){
       char c = evt.getKeyChar(); // Get the typed character 
        if (c != KeyEvent.VK_BACK_SPACE && c != KeyEvent.VK_DELETE) {
            if(!(Character.isDigit(c))){
                if ((c == '.')) {
                    if (txt.getText().indexOf(".") < 0)
                        return;
                    else{ 
                        evt.consume(); //Ignore this key
                        return;
                    } 
                } 
                else{
                    evt.consume(); // Ignore this key 
                    return;
                }
            } 
        }
    }
    private Collection data(){
        ArrayList<DataConllection>arr=new ArrayList<>();
        DecimalFormat df=new DecimalFormat("$0.00");
        for(int i=0;i<table.getRowCount();i++){
            String name=""+table.getValueAt(i, 4);
            String type=""+table.getValueAt(i,5);
            String day=""+table.getValueAt(i, 9);
            //String price = "" + NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(table.getValueAt(i, 6));
            String price=""+table.getValueAt(i, 6);
            price=df.format(Double.valueOf(price));
            String prepaid="$"+table.getValueAt(i, 10);
            String amount = "" + NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(table.getValueAt(i, 11));
            DataConllection obj=new DataConllection(name,type,day,price,prepaid,amount);
            arr.add(obj);
        }
        return arr;
    }
    private void searchComboName(){
     if(cboName.getSelectedItem()!=null){
           try{
            String sql="Select *From vguestdetail where guname=?";
            pst=ConnectDB.con.prepareStatement(sql);
            pst.setString(1,cboName.getSelectedItem().toString());
            mySearch(sql,pst);
            myPayment();
            }catch(SQLException e){e.printStackTrace();}
        }else return;
    }    public void searchCombo(){
        if(cboName.getSelectedItem()!=null){
           try{
            String sql="Select *From vguestdetail where guname=?";
            pst=ConnectDB.con.prepareStatement(sql);
            pst.setString(1,cboName.getSelectedItem().toString());
            mySearch(sql,pst);
            myPayment();
            }catch(SQLException e){e.printStackTrace();}
        }else return;
    }
    private void searchName(){
        if(cboName.getSelectedItem()!=null){
           try{
            String sql="Select *From vguestdetail where guname=?";
            pst=ConnectDB.con.prepareStatement(sql);
            pst.setString(1,cboName.getSelectedItem().toString());
            mySearch(sql,pst);
            myPayment();
            }catch(SQLException e){e.printStackTrace();}
        }else return;
    }
    
    public String systemDate(){
         java.util.Date cd=new java.util.Date();
            String d=new SimpleDateFormat("dd").format(cd);
            String m=new SimpleDateFormat("MM").format(cd);
            String y=new SimpleDateFormat("yyyy").format(cd);   
            String date=y+"-"+m+"-"+d;
            return date;
    }
    public void mySearch(String sql,PreparedStatement pst)throws SQLException{
        while(model.getRowCount()>0){
                        for(int j=0;j<model.getRowCount();j++)
                        model.removeRow(j);
        }
        String date=systemDate();
            r=pst.executeQuery();
            while(r.next()){
              model.addRow(new Object[]{r.getString("guName"),r.getString("idc"),r.getString("contact"),
                        r.getString("guadd"),r.getString("rname"),r.getString("rttype"),
                        r.getString("rtprice"),r.getString("checkin"),r.getString("checkout"),r.getString("dns"),
                        Double.parseDouble(r.getString("prepaid")),Double.parseDouble(r.getString("amount")),r.getString("idran")
                });
            }
       
        
   }
    public void myPayment(){
        double amount=0,prepaid=0;
        String name="",invoice="",phone="",idran="";
        for(int i=0;i<table.getRowCount();i++){
            double a=(Double) model.getValueAt(i,11);
            amount+=a;
            double b=(Double) model.getValueAt(i, 10);
            prepaid+=b;
            name=(String)model.getValueAt(i,0);
            invoice=(String)model.getValueAt(i, 12);
            phone=(String)model.getValueAt(i, 2);
            idran=""+model.getValueAt(i, 12);
        }
        
        double tot=0;
            if(amount>prepaid)   tot=amount-prepaid;
            else if(amount<prepaid)  tot=prepaid-amount;
        txtPrepaid.setText(""+prepaid);
        txtAmount.setText(""+amount);
        txtTotal.setText(""+tot);
        
        txtTotalR.setText(df.format(tot*4100)+"");
       
        lblInvoice2.setText(invoice);
        lblPhone.setText(phone); 
    }
//    public void addCombo(){
//        ArrayList<String>list=combo();
//      //  cboRname.addItem("Choose");
//        for(int i=0;i<list.size();i++){
//            String a=list.get(i);
//            cboRname.addItem(a);
//        }
//    }
    public ArrayList<String> combo(){
        String date=systemDate();String phone="";
        ArrayList<String>list=new ArrayList();
        String sql="Select idc from vguestdetail where checkin <=? and checkout>= ? order by idc";
        try{
            pst=ConnectDB.con.prepareStatement(sql);
            pst.setString(1,date);
            pst.setString(2,date);
            r=pst.executeQuery();
            while(r.next()){
                phone=r.getString("idc");
                list.add(phone);
            }
        }catch(SQLException e){e.printStackTrace();}
        return list;
    }
    private ArrayList<String> comboName(){
        String date=systemDate();String name="";
        ArrayList<String>l=new ArrayList();
        String sql="Select Distinct  guname from vguestdetail where checkin <=? and checkout>= ? order by guname";
        try{
            pst=ConnectDB.con.prepareStatement(sql);
            pst.setString(1,date);
            pst.setString(2,date);
            r=pst.executeQuery();
            while(r.next()){
                name=r.getString("guname");
              // JOptionPane.showMessageDialog(null,r.getString("guname"));
                l.add(name);
            }
        }catch(SQLException e){e.printStackTrace();}
        return l;
    }
    
    public void addComboname(){
        ArrayList<String>l=comboName();
      //  cboRname.addItem("Choose");
        for(int i=0;i<l.size();i++){
            String a=l.get(i);
            cboName.addItem(a);
        }
    }
    public void myClear(){
        lblInvoice2.setText("");
        txtAmount.setText("");
        txtPrepaid.setText("");
        txtTotal.setText("");
        lblPhone.setText("");
    }
    public void table(){
        model.addColumn("Name");    
        model.addColumn("Identify Card");
        model.addColumn("Contact");
        model.addColumn("Address");
        model.addColumn("Room Name");
        model.addColumn("Room Type");
        model.addColumn("Price");
        model.addColumn("CheckIn");
        model.addColumn("CheckOut");
        model.addColumn("Day");
        model.addColumn("Prepaid");
        model.addColumn("Total");
        model.addColumn("Invoice");
        table.setModel(model);
        table.getColumnModel().getColumn(0).setPreferredWidth(120);
        table.getColumnModel().getColumn(1).setPreferredWidth(80);
        table.getColumnModel().getColumn(2).setPreferredWidth(80);
        table.getColumnModel().getColumn(3).setPreferredWidth(100);
        table.getColumnModel().getColumn(4).setPreferredWidth(90);
        table.getColumnModel().getColumn(5).setPreferredWidth(90);
        table.getColumnModel().getColumn(6).setPreferredWidth(70);
        table.getColumnModel().getColumn(7).setPreferredWidth(80);
        table.getColumnModel().getColumn(8).setPreferredWidth(80);
        table.getColumnModel().getColumn(9).setPreferredWidth(50);
        table.getColumnModel().getColumn(10).setPreferredWidth(70);
        table.getColumnModel().getColumn(11).setPreferredWidth(70);
        table.getColumnModel().getColumn(12).setPreferredWidth(100);
        table.removeColumn(table.getColumnModel().getColumn(12));

    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Payment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Payment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Payment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Payment.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Payment().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnBack;
    public static javax.swing.JButton btnClose;
    private javax.swing.JButton btnPrint;
    private javax.swing.JComboBox<String> cboName;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblInvoice2;
    private javax.swing.JLabel lblNum1;
    private javax.swing.JLabel lblNum10;
    private javax.swing.JLabel lblNum12;
    private javax.swing.JLabel lblNum2;
    private javax.swing.JLabel lblNum3;
    private javax.swing.JLabel lblNum5;
    private javax.swing.JLabel lblNum6;
    private javax.swing.JLabel lblNum7;
    private javax.swing.JLabel lblNum9;
    private javax.swing.JLabel lblPhone;
    private javax.swing.JTable table;
    private javax.swing.JTextField txtAmount;
    private javax.swing.JTextField txtPrepaid;
    private javax.swing.JTextField txtReceivce;
    private javax.swing.JTextField txtReturn;
    private javax.swing.JTextField txtReturnR;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtTotalR;
    // End of variables declaration//GEN-END:variables
    private DefaultTableModel model=new DefaultTableModel();
}
