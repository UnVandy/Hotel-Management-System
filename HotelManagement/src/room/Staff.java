package room;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.sql.PreparedStatement;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.Year;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.NumberFormatter;

public class Staff extends javax.swing.JFrame {
    private PreparedStatement pst;
    private Statement st;
    private ResultSet rs;
    private SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
    private String path;
    private String filename;
    private int len;
    private byte[] imagedata;
    private static double amount=0;
    private FileInputStream fis;
    private File f;
    DecimalFormat df=new DecimalFormat("$000.00");
    public Staff() {
        initComponents();
        setLocationRelativeTo(null);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        txtPhone = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        rFemale = new javax.swing.JRadioButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtAddress = new javax.swing.JTextField();
        rMale = new javax.swing.JRadioButton();
        txtSalary = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        btnBrowse = new javax.swing.JButton();
        txtName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtPosition = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblImage = new javax.swing.JLabel();
        dob = new com.toedter.calendar.JDateChooser();
        Hiredate = new com.toedter.calendar.JDateChooser();
        jPanel2 = new javax.swing.JPanel();
        btnSave = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnNew = new javax.swing.JButton();
        txtID = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        txtSearch = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(204, 255, 204));

        jPanel1.setBackground(new java.awt.Color(153, 0, 204));

        txtPhone.setFont(new java.awt.Font("Kh Content", 0, 18)); // NOI18N
        txtPhone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPhoneKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPhoneKeyTyped(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Telephone:");

        buttonGroup1.add(rFemale);
        rFemale.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        rFemale.setText("Female");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setText("Position:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel8.setText("Salary:");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel9.setText("HireDate:");

        txtAddress.setFont(new java.awt.Font("Kh Content", 0, 18)); // NOI18N
        txtAddress.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAddressKeyPressed(evt);
            }
        });

        buttonGroup1.add(rMale);
        rMale.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        rMale.setText("Male");

        txtSalary.setFont(new java.awt.Font("Kh Content", 0, 18)); // NOI18N
        txtSalary.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtSalaryFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtSalaryFocusLost(evt);
            }
        });
        txtSalary.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSalaryKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSalaryKeyTyped(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Address:");

        btnBrowse.setBackground(new java.awt.Color(255, 255, 255));
        btnBrowse.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/Browse.png"))); // NOI18N
        btnBrowse.setText("Browse");
        btnBrowse.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnBrowseMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnBrowseMouseExited(evt);
            }
        });
        btnBrowse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBrowseActionPerformed(evt);
            }
        });

        txtName.setFont(new java.awt.Font("Kh Content", 0, 18)); // NOI18N
        txtName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNameFocusLost(evt);
            }
        });
        txtName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNameKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNameKeyTyped(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("Sex:");

        txtPosition.setFont(new java.awt.Font("Kh Content", 0, 18)); // NOI18N
        txtPosition.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPositionKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPositionKeyTyped(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Name:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Date of Birth:");

        lblImage.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(rMale))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(rFemale, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addGap(18, 18, 18)
                            .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(32, 32, 32)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel7)
                                        .addGap(13, 13, 13)
                                        .addComponent(txtPosition, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel8)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel9)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(Hiredate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 0, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(83, 83, 83)
                        .addComponent(txtAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBrowse)
                        .addGap(13, 13, 13)))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel2, jLabel3, jLabel4, jLabel6});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtAddress, txtName, txtPhone, txtPosition, txtSalary});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(42, 42, 42)
                                        .addComponent(jLabel7))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel5))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtPosition, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(9, 9, 9)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel8)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel9))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(txtSalary, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(Hiredate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBrowse))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(rMale)
                                    .addComponent(rFemale)))
                            .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3)))
                            .addComponent(dob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel2, jLabel3, jLabel4, jLabel5, jLabel6, jLabel8, jLabel9, rFemale, rMale, txtName, txtPhone});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txtAddress, txtPosition, txtSalary});

        jPanel2.setBackground(new java.awt.Color(153, 153, 255));

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/Save-icon.png"))); // NOI18N
        btnSave.setText("Save");
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveMouseExited(evt);
            }
        });
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnEdit.setBackground(new java.awt.Color(255, 255, 255));
        btnEdit.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/Edit-validated-icon.png"))); // NOI18N
        btnEdit.setText("Edit");
        btnEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEditMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnEditMouseExited(evt);
            }
        });
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnDelete.setBackground(new java.awt.Color(255, 255, 255));
        btnDelete.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/Actions-edit-delete-icon.png"))); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDeleteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnDeleteMouseExited(evt);
            }
        });
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnNew.setBackground(new java.awt.Color(255, 255, 255));
        btnNew.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        btnNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/Folder-Add-icon.png"))); // NOI18N
        btnNew.setText("New");
        btnNew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnNewMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnNewMouseExited(evt);
            }
        });
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(153, 153, 153)
                .addComponent(btnNew, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEdit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnDelete, btnEdit, btnNew, btnSave});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnNew)
                .addComponent(btnSave)
                .addComponent(btnDelete)
                .addComponent(btnEdit)
                .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel20.setFont(new java.awt.Font("Segoe Print", 3, 36)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(153, 0, 0));
        jLabel20.setText("Staff Information ");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/back.png"))); // NOI18N
        jButton1.setBorderPainted(false);
        jButton1.setContentAreaFilled(false);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        txtSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSearch.setForeground(new java.awt.Color(204, 204, 204));
        txtSearch.setText("Search here");
        txtSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtSearch.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtSearchFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtSearchFocusLost(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });

        table.setForeground(new java.awt.Color(0, 51, 255));
        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Name", "Sex", "Date of Birth", "Phone number", "Address", "Position", "Salary", "HireDate", "StopWork"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        table.setFocusable(false);
        table.setRequestFocusEnabled(false);
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(table);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(165, 165, 165)
                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jScrollPane2)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSearch, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
     private void formLoad(){
        initTable();
        myClear();
        noCusor();       
        txtID.setVisible(false);
        buttonOff();
    }
    private void buttonOff(){
        btnSave.setText("Save");
        btnSave.setEnabled(false);
        btnEdit.setText("Edit");
        btnEdit.setEnabled(false);
        btnDelete.setEnabled(false);
    }
    private void buttonOn(String b1){
        btnSave.setEnabled(true);
        btnEdit.setEnabled(true);
        btnDelete.setEnabled(true);
        btnSave.setText("Save");
        btnEdit.setText("Edit");
    }
     private void getCusor(){
        txtName.setFocusable(true);
        txtAddress.setFocusable(true);
        txtPhone.setFocusable(true);
        txtPosition.setFocusable(true);
        txtSalary.setFocusable(true);    
        Hiredate.setEnabled(true);
        dob.setEnabled(true);
        txtName.requestFocus();
    }
    private void noCusor(){
        txtName.setFocusable(false);
        txtAddress.setFocusable(false);
        txtPhone.setFocusable(false);
        txtPosition.setFocusable(false);
        txtSalary.setFocusable(false); 
        dob.setEnabled(false);
        Hiredate.setEnabled(false);
    }
    private void myClear(){
        txtName.setText("");
        rMale.setSelected(false);
        rMale.setSelected(false);
        txtAddress.setText("");
        txtPhone.setText("");
        txtPosition.setText("");
        txtSalary.setText("");
        dob.setDate(null);
        Hiredate.setDate(null);
        lblImage.setIcon(null);
        txtName.requestFocus();
    }
     private void getNew(){
        if(btnNew.getText()=="New"){
           // btnNew.setIcon(new ImageIcon(.getClass().getResource("book/picture/cancel.jpg")));
            btnNew.setIcon(new ImageIcon(getClass().getResource("Picture\\Cancel.png"))); 
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png")));
            getCusor();
            btnNew.setText("Cancel");
            myClear();
            buttonOff();
            btnSave.setEnabled(true);
            amount=0;
        }else if(btnNew.getText()=="Cancel"){
            int a=JOptionPane.showConfirmDialog(null,"Do you want to cancel ?","Comfirm",JOptionPane.YES_NO_OPTION);
            if(JOptionPane.YES_OPTION==a){
            btnNew.setIcon(new ImageIcon(getClass().getResource("Picture\\Folder-Add-icon.png")));
            myClear();
            noCusor();
            btnNew.setText("New");
            btnSave.setEnabled(false);
            amount=0;
            }
        }
    }
    private void getEdit(){
        if(btnEdit.getText()=="Edit"){
            getCusor();
            btnSave.setText("Update");
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Cancel.png"))); 
            btnEdit.setText("Cancel");
            btnSave.setEnabled(true);
        }else if(btnEdit.getText()=="Cancel"){
            myClear();
            noCusor();
            btnEdit.setText("Edit");
            btnSave.setText("Save");
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png"))); 
            buttonOff();
        }
    }
    private void getUpdate(){
        if(btnSave.getText()=="Save"){
            mySave();
            myClear();
        }else if(btnSave.getText()=="Update"){
            myUpdate();
            myClear();
            buttonOff();
            noCusor();
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png")));
        }
    }
    private void numberKey(KeyEvent evt,JTextField txt){
       char c = evt.getKeyChar(); // Get the typed character 
        if (c != KeyEvent.VK_BACK_SPACE && c != KeyEvent.VK_DELETE) {
            if(!(Character.isDigit(c))){
                if ((c == '.')) {
                    if (txt.getText().indexOf(".") < 0)
                        return;
                    else{ 
                        evt.consume(); //Ignore this key
                        return;
                    } 
                } 
                else{
                    evt.consume(); // Ignore this key 
                    return;
                }
            } 
        }
    }
    private void keyEnter(KeyEvent e,JTextField t){
        if(e.getKeyCode()==KeyEvent.VK_ENTER)
            t.requestFocus();
    }
    private boolean checkValue(){
        boolean b=false;
        if(txtName.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Please input staff's 'Name'","Missing",JOptionPane.WARNING_MESSAGE);txtName.requestFocus();b=true;
        }else if(rMale.isSelected()==false && rFemale.isSelected()==false){
            JOptionPane.showMessageDialog(null,"Please choose staff's 'Sex'","Missing",JOptionPane.WARNING_MESSAGE);b=true;
        }else if(dob.getDate()==null){
            JOptionPane.showMessageDialog(null,"Please choose staff's 'Birthday'","Missing",JOptionPane.WARNING_MESSAGE);b=true;
        }else if(txtPhone.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Please input staff's 'Phone'","Missing",JOptionPane.WARNING_MESSAGE);txtPhone.requestFocus();b=true;
        }else if(txtAddress.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Please input staff's 'Address'","Missing",JOptionPane.WARNING_MESSAGE);txtAddress.requestFocus();b=true;
        }else if(txtPosition.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Please input staff's 'Position'","Missing",JOptionPane.WARNING_MESSAGE);txtPosition.requestFocus();b=true;
        }else if(txtSalary.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Please input staff's 'Salary'","Missing",JOptionPane.WARNING_MESSAGE);txtSalary.requestFocus();b=true;
        }else if(Hiredate.getDate()==null){
            JOptionPane.showMessageDialog(null,"Please choose 'Hiredate'","Missing",JOptionPane.WARNING_MESSAGE);b=true;
        }else if(lblImage.getIcon()==null){
            JOptionPane.showMessageDialog(null,"Please choose staff's 'Photo'","Missing",JOptionPane.WARNING_MESSAGE);btnBrowse.doClick();b=true;
        }else b=false;
        amount=0;
        return b;
    }
    private void txtNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNameKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            rMale.setSelected(true);
        }
    }//GEN-LAST:event_txtNameKeyPressed

    private void txtPhoneKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPhoneKeyPressed
        keyEnter(evt, txtAddress);
    }//GEN-LAST:event_txtPhoneKeyPressed

    private void txtAddressKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAddressKeyPressed
        keyEnter(evt,txtPosition);
    }//GEN-LAST:event_txtAddressKeyPressed

    private void txtPositionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPositionKeyPressed
        keyEnter(evt,txtSalary);
    }//GEN-LAST:event_txtPositionKeyPressed

    private void txtSalaryKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSalaryKeyPressed
//        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
//        Hiredate.getDateEditor().getUiComponent().requestFocusInWindow();
//        Hiredate.setCalendar(null);}
//              }
        int kp = evt.getKeyCode();
        if(kp==KeyEvent.VK_ENTER){
            
            //amount = Double.valueOf(txtSalary.getText());
            //txtSalary.setText(NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(amount));
            
        }
    }//GEN-LAST:event_txtSalaryKeyPressed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        if(checkValue()==false)
            getUpdate();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void txtSearchFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSearchFocusGained
        // TODO add your handling code here:
        txtSearch.setText("");
        //btnNew.setText(setNew);
    }//GEN-LAST:event_txtSearchFocusGained

    private void txtSearchFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSearchFocusLost
        txtSearch.setForeground(Color.DARK_GRAY);
        txtSearch.setText("Search here");
    }//GEN-LAST:event_txtSearchFocusLost

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        mySearch();
    }//GEN-LAST:event_txtSearchKeyPressed
    
    private void btnBrowseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBrowseActionPerformed
        BrowseImage();
    }//GEN-LAST:event_btnBrowseActionPerformed

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        getNew();
    }//GEN-LAST:event_btnNewActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        MainForm.main(null);
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
        tableClick();
    }//GEN-LAST:event_tableMouseClicked

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        myDelete();
        myClear();
        noCusor();
        buttonOff();
        btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png")));
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
      //  JOptionPane.showMessageDialog(null,amount);
        getEdit();
    }//GEN-LAST:event_btnEditActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        formLoad();
    }//GEN-LAST:event_formWindowOpened

    private void txtNameKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNameKeyTyped
        char ch=evt.getKeyChar();
        if(Character.isDigit(ch)
            || (ch==KeyEvent.VK_BACK_SLASH)
            || (ch==KeyEvent.VK_DELETE)){
        getToolkit().beep();
        evt.consume();
        }
    }//GEN-LAST:event_txtNameKeyTyped

    private void txtPhoneKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPhoneKeyTyped
        int max=9;
        char ch = evt.getKeyChar();
        int a =txtPhone.getText().length();
        if(a > max)
        {
            getToolkit().beep();
            evt.consume();
        }
        if(!(Character.isDigit(ch))
            || (ch==KeyEvent.VK_BACK_SLASH)
            || (ch==KeyEvent.VK_DELETE)){
        evt.consume();
        }
    }//GEN-LAST:event_txtPhoneKeyTyped

    private void txtSalaryKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSalaryKeyTyped
        numberKey(evt,txtSalary);
    }//GEN-LAST:event_txtSalaryKeyTyped

    private void dobPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_dobPropertyChange
        txtPhone.requestFocus();
    }//GEN-LAST:event_dobPropertyChange

    private void txtPositionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPositionKeyTyped
        char ch=evt.getKeyChar();
        if(Character.isDigit(ch)
            || (ch==KeyEvent.VK_BACK_SLASH)
            || (ch==KeyEvent.VK_DELETE)
            || (ch=='.'))   {
        getToolkit().beep();
        evt.consume();
        }
    }//GEN-LAST:event_txtPositionKeyTyped

    private void txtNameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNameFocusLost
 
    }//GEN-LAST:event_txtNameFocusLost

    private void txtSalaryFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSalaryFocusLost
        if(txtSalary.getText().equals("")) return;
        else {
        String a=txtSalary.getText();       
        txtSalary.setText(NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(Double.valueOf(a)));
       //JOptionPane.showMessageDialog(null,a);
        }
    }//GEN-LAST:event_txtSalaryFocusLost

    private void txtSalaryFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSalaryFocusGained
         String a=txtSalary.getText();
        if(a.equals("")) return;
        double b=Double.valueOf(a.replaceAll("[^\\d.]+",""));
        txtSalary.setText(b+""); 
    }//GEN-LAST:event_txtSalaryFocusGained

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try {
            Robot robot=new Robot();
                robot.keyPress(KeyEvent.VK_UP);
        } catch (AWTException ex) {
           ex.printStackTrace();
        }
        
    }//GEN-LAST:event_txtSearchKeyReleased

    private void btnNewMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNewMouseEntered
        btnNew.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnNewMouseEntered

    private void btnSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseEntered
        btnSave.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnSaveMouseEntered

    private void btnDeleteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMouseEntered
        btnDelete.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnDeleteMouseEntered

    private void btnEditMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditMouseEntered
        btnEdit.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnEditMouseEntered

    private void btnNewMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNewMouseExited
        btnNew.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnNewMouseExited

    private void btnSaveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseExited
        btnSave.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnSaveMouseExited

    private void btnDeleteMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMouseExited
        btnDelete.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnDeleteMouseExited

    private void btnEditMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditMouseExited
        btnEdit.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnEditMouseExited

    private void btnBrowseMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBrowseMouseEntered
        btnBrowse.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnBrowseMouseEntered

    private void btnBrowseMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnBrowseMouseExited
        btnBrowse.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnBrowseMouseExited
    private void mySave(){
        try{
        getMySave("{call insertStaff(?,?,?,?,?,?,?,?,?,?)}");
        pst.executeUpdate();
        JOptionPane.showMessageDialog(null,"Saved Successfully!");
        toTable();
        }catch(SQLException e){e.printStackTrace();}
    }
    private void myUpdate(){
        try{
        getMySave("{call updateStaff(?,?,?,?,?,?,?,?,?,?,?)}");
        pst.setInt(11,Integer.parseInt(txtID.getText()));
        pst.executeUpdate();
        JOptionPane.showMessageDialog(null,"Updated Successfully!");
        toTable();
        }catch(SQLException e){e.printStackTrace();}
    }
    private void myDelete(){
        String sql="{call deleteStaff(?)}";
        try{
            int i=JOptionPane.showConfirmDialog(null,"Are you sure want to delete?","Comfirm",JOptionPane.YES_NO_OPTION);
            if(JOptionPane.YES_OPTION==i){
            pst=ConnectDB.con.prepareStatement(sql);
            pst.setInt(1,Integer.parseInt(txtID.getText()));
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null,"Delete successfully");
            model.removeRow(table.getSelectedRow());
            }
        }catch(SQLException e){e.printStackTrace();JOptionPane.showMessageDialog(null,"Error Unable to delete!","Message",JOptionPane.ERROR_MESSAGE);}
    }
    private void mySearch(){
        txtSearch.setForeground(Color.BLACK);
        while(model.getRowCount()>0){
            for(int i=0;i<model.getRowCount();i++){
                model.removeRow(i);
            }
        }
        String sql="{call searchStaff(?,?)}";
        String s=txtSearch.getText().trim();
        try{
            pst=ConnectDB.con.prepareStatement(sql);
            pst.setString(1,s);
           // pst.setInt(2,Integer.parseInt(s));
            pst.setString(2,s);
            rs=pst.executeQuery();
            while(rs.next()){
                model.addRow(new Object[]{rs.getString("stID"),rs.getString("stName"),rs.getString("Sex"),
                sdf.format(rs.getDate("dob")),rs.getString("stPhone"),rs.getString("stAdd"),rs.getString("stPos"),
                rs.getString("Salary"),sdf.format(rs.getDate("Hireddate")),rs.getString("Photo")});
            }            
        }catch(SQLException e){e.printStackTrace();}
    }
    private void tableClick(){
        
       String sa;
        try{
        String sex="";
        txtPosition.requestFocus();
        txtID.setText(model.getValueAt(table.getSelectedRow(),0).toString());
        txtName.setText(model.getValueAt(table.getSelectedRow(),1).toString());
        sex=(model.getValueAt(table.getSelectedRow(),2).toString());
        //JOptionPane.showMessageDialog(null,sex);
        if(sex.equalsIgnoreCase("M"))rMale.setSelected(true);
        if(sex.equalsIgnoreCase("F"))rFemale.setSelected(true);
        Date d1=new SimpleDateFormat("dd-MM-yyyy").parse((String)model.getValueAt(table.getSelectedRow(),3).toString());
        dob.setDate(d1);
        txtPhone.setText(model.getValueAt(table.getSelectedRow(),4).toString());
        txtAddress.setText(model.getValueAt(table.getSelectedRow(),5).toString());
        txtPosition.setText(model.getValueAt(table.getSelectedRow(),6).toString());
        String a=model.getValueAt(table.getSelectedRow(), 7).toString();
//        StringTokenizer t=new StringTokenizer(a,"$");
//        String b=t.nextToken();
        txtSalary.setText(a);
        Date d2=new SimpleDateFormat("dd-MM-yyyy").parse((String)model.getValueAt(table.getSelectedRow(),8).toString());
        Hiredate.setDate(d2);
         String Sql = "{Call getImageFromStaff(?)}";
            pst = ConnectDB.con.prepareStatement(Sql);
            pst.setInt(1,Integer.parseInt(txtID.getText()));
            rs = pst.executeQuery();
            if(rs.next()){
                imagedata = rs.getBytes("Photo");
                if(imagedata==null){
                    //lblImage.setIcon(new ImageIcon(getClass().getResource("Picture\\Person-Male-Light-icon")));
                    
                    //lblImage.setIcon(new ImageIcon(getClass().getResource(null)));
                    //lblImage.setText("No Picture to preview");
                   //JOptionPane.showMessageDialog(null,"No picture");
                }else {
                    lblImage.setIcon(new ImageIcon (Toolkit.getDefaultToolkit().createImage(imagedata).getScaledInstance(lblImage.getWidth(), lblImage.getHeight(), Image.SCALE_SMOOTH)));
                }
            }
        noCusor();
        Hiredate.setFocusable(false);
        btnDelete.setEnabled(true);
        btnSave.setEnabled(false);
        btnEdit.setEnabled(true);
        btnEdit.setText("Edit");
        btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png")));
        btnNew.setIcon(new ImageIcon(getClass().getResource("Picture\\Folder-Add-icon.png")));
        btnNew.setText("New");
        }catch(Exception e){e.printStackTrace();JOptionPane.showMessageDialog(null,"Error");}
    }
    private void BrowseImage(){
    JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Choose an image");
        fc.setAcceptAllFileFilterUsed(true); //All files type
        FileNameExtensionFilter filter = new FileNameExtensionFilter("PNG File", "png");
        FileNameExtensionFilter filter1 = new FileNameExtensionFilter("JPEG File", "jpg", "jpeg");
        FileNameExtensionFilter filter2 = new FileNameExtensionFilter("BMP Image", "bmp");
        
        fc.addChoosableFileFilter(filter);
        fc.addChoosableFileFilter(filter1);
        fc.addChoosableFileFilter(filter2);
        
        int result = fc.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            f = fc.getSelectedFile();
            filename = f.getAbsolutePath();
            try{
                filename = filename.replace("\\", "\\\\");
                int w = lblImage.getWidth(), h = lblImage.getHeight();
                lblImage.setIcon(new ImageIcon(new ImageIcon(filename).getImage().getScaledInstance(w, h, Image.SCALE_SMOOTH)));
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex.getMessage());
            }
        }
    }                                         
    private void getMySave(String sql){
       // sql="{call insertStaff(?,?,?,?,?,?,?,?,?,?)}";
        String sex="";String sa="";float ab;
        StringTokenizer t1;
        String a=txtSalary.getText();
        if(a.equals("")) return;
        else {
             ab=Float.valueOf(a.replaceAll("[^\\d.]+",""));
        }
        if(rMale.isSelected()){
            sex="M";
        }else if(rFemale.isSelected())
            sex="F";
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        sdf.format(dob.getDate());
        try{
            pst=ConnectDB.con.prepareStatement(sql);
            pst.setString(1,txtName.getText());
            pst.setString(2,sex);
            pst.setString(3,sdf.format(dob.getDate()));
            pst.setString(4,txtPosition.getText());
            pst.setFloat(5,ab);
            pst.setString(6,txtAddress.getText());
            pst.setString(7,txtPhone.getText());
            pst.setString(8,sdf.format(Hiredate.getDate()));
            pst.setInt(9,1);
            if(filename!=null){
                fis = new FileInputStream(f);
                len = (int)f.length();
                pst.setBinaryStream(10, fis, len);
            }
            else{
                pst.setBinaryStream(10, new ByteArrayInputStream(imagedata), imagedata.length);
            }
        }catch(Exception e){e.printStackTrace();JOptionPane.showMessageDialog(null,"Save not completed at=>"+e);}
    }
    private void initTable(){
        model.addColumn("ID");
        model.addColumn("Name");
        model.addColumn("Sex");
        model.addColumn("Birthday");
        model.addColumn("Phone Number");
        model.addColumn("Address");
        model.addColumn("Position");
        model.addColumn("Salary");
        model.addColumn("HiredDate");
        model.addColumn("Photo");
        table.setModel(model);
        table.getColumnModel().getColumn(0).setPreferredWidth(40);
        table.getColumnModel().getColumn(1).setPreferredWidth(140);
        table.getColumnModel().getColumn(2).setPreferredWidth(40);
        table.getColumnModel().getColumn(3).setPreferredWidth(105);
        table.getColumnModel().getColumn(4).setPreferredWidth(110);
        table.getColumnModel().getColumn(5).setPreferredWidth(130);
        table.getColumnModel().getColumn(6).setPreferredWidth(130);
        table.getColumnModel().getColumn(7).setPreferredWidth(80);
        table.getColumnModel().getColumn(8).setPreferredWidth(105);
        table.getColumnModel().getColumn(9).setPreferredWidth(50);
        table.removeColumn(table.getColumnModel().getColumn(9));
        toTable();
    }
    private void toTable(){
        //txtSalary.setText(NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(amount));
        while(model.getRowCount()>0){
            for(int i=0;i<model.getRowCount();i++){
                model.removeRow(i);
            }
        }
        DecimalFormat money=new DecimalFormat("$000.00");
        String sql="{call selectStaff(?)}";
        try{
            pst=ConnectDB.con.prepareStatement(sql);
            pst.setInt(1,1);
            rs=pst.executeQuery();
            while(rs.next()){
                amount=rs.getDouble("Salary");
                model.addRow(new Object[]{rs.getString("stID"),rs.getString("stName"),rs.getString("Sex"),
                sdf.format(rs.getDate("dob")),rs.getString("stPhone"),rs.getString("stAdd"),rs.getString("stPos"),
                 NumberFormat.getCurrencyInstance(new Locale("en", "US")).format(amount),
                //money.format(amount),
                sdf.format(rs.getDate("Hireddate")),rs.getString("Photo")});
            }
        }catch(SQLException e){e.printStackTrace();}
    }
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Staff().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser Hiredate;
    private javax.swing.JButton btnBrowse;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnSave;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private com.toedter.calendar.JDateChooser dob;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblImage;
    private javax.swing.JRadioButton rFemale;
    private javax.swing.JRadioButton rMale;
    private javax.swing.JTable table;
    private javax.swing.JTextField txtAddress;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPhone;
    private javax.swing.JTextField txtPosition;
    private javax.swing.JTextField txtSalary;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
    private DefaultTableModel model=new DefaultTableModel();
}
