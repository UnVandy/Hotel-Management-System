package room;

public class CRoomType {
    private int id;
    private String type;
    private String size;
    private double price;
    private String des;
    
    public CRoomType(){
        this(3,"VIP","Small",5,"3");
    }
    public CRoomType(int id,String type, String size, double price,String des) {
        this.id=id;
        this.type = type;
        this.size = size;
        this.price = price;
        this.des=des;
    }
    public int getId(){return id;}
    public String getType() {return type;}
    public String getSize() {return size;}
    public double getPrice() {return price;}
    public String getDes(){return des;}

    public void setType(String type) {this.type = type;}
    public void setSize(String size) {this.size = size;}
    public void setPrice(double price) {this.price = price;}
    public void setDes(String des){this.des=des;}
    @Override
    public String toString() {
        return "CRoomType{" + "id=" + id + ", type=" + type + ", size=" + size + ", price=" + price + '}';
    }
  
   
    
    
    
}
