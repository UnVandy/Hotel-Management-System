package room;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JFrame;
import javax.swing.JPanel;

public  class MoveText extends JPanel{
     int x=0,y=100;
     public void paint(Graphics g){
       
        super.paint(g);
        Graphics2D g2 = (Graphics2D)g;
        Font font=new Font("Tahoma",Font.BOLD+Font.PLAIN,100);
        g2.setFont(font);
        g2.setColor(Color.yellow);
        g2.drawString("Welcome",x,y);
        try{
            Thread.sleep(100);
        }catch(Exception e){e.printStackTrace();}
        x+=10;
        if(x>this.getWidth()){
            x=0;
        }
        repaint();
    }
//     public static void main(String args[]){
//         JFrame jf=new JFrame("EAST");
//         jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//         jf.setSize(700,200);
//         jf.add(new MoveText());
//         jf.setVisible(true);
//     }
}
