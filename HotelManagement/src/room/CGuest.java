package room;

public class CGuest {
    private int id;
    private String name;
    private String rttype;
    private double price;
    private String size;
    private String chin;
    private String chout;
    private String contact;
    private String card;
    private String add;
    private int dns;
    private double prepaid;
    private double amount;
    private String idran;
    private String des;
    private String rname;

    public CGuest() {
    }

    
    public CGuest(int id, String name, String rttype,String rname, double price,String des, String size, String chin, String chout, String contact,String card,String add, int dns, double prepaid, double amount,String idran) {
        this.id = id;
        this.name = name;
        this.rttype = rttype;
        this.price = price;
        this.size = size;
        this.chin = chin;
        this.chout = chout;
        this.contact = contact;
        this.card=card;
        this.add=add;
        this.dns = dns;
        this.des=des;
        this.prepaid = prepaid;
        this.amount = amount;
        this.idran=idran;
        this.rname=rname;
    }

    public int getId() {
        return id;
    }
    public String getAdd(){
        return add;
    }
    public String getCard(){
        return card;
    }
    public String getName() {
        return name;
    }

    public String getRttype() {
        return rttype;
    }

    public double getPrice() {
        return price;
    }

    public String getSize() {
        return size;
    }

    public String getChin() {
        return chin;
    }

    public String getChout() {
        return chout;
    }

    public String getContact() {
        return contact;
    }

    public int getDns() {
        return dns;
    }

    public double getPrepaid() {
        return prepaid;
    }

    public double getAmount() {
        return amount;
    }
    public String getIdran(){
        return idran;
    }
    public String getDes(){
        return des;
    }
    public String getRName(){
        return rname;
    }
    public void setIdran(String rid){
        this.idran=rid;
    }
    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRttype(String rttype) {
        this.rttype = rttype;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setChin(String chin) {
        this.chin = chin;
    }

    public void setChout(String chout) {
        this.chout = chout;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public void setDns(int dns) {
        this.dns = dns;
    }

    public void setPrepaid(double prepaid) {
        this.prepaid = prepaid;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "CGuest{" + "id=" + id + ", name=" + name + ", rttype=" + rttype + ", price=" + price + ", size=" + size + ", chin=" + chin + ", chout=" + chout + ", contact=" + contact + ", card=" + card + ", add=" + add + ", dns=" + dns + ", prepaid=" + prepaid + ", amount=" + amount + ", idran=" + idran + ", des=" + des + ", rname=" + rname + '}';
    }

   
    
}
