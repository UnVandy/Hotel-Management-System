package room;
import java.sql.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import static java.lang.System.exit;
import java.util.Random;
import java.util.Vector;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import room.CRoomType;

public class RoomType extends javax.swing.JFrame {
    private Statement st;
    private ResultSet r;
    private String cmdSave="Save";
    private String cmdUpdate="Update";
    private String cmdEdit="Edit";
    private String cmdCancel="Cancel";
    private String cmdNew="New";
      public RoomType() {
        initComponents();
        setLocationRelativeTo(null);
        formLoad();
    }

    private void formLoad(){
        initTable();
        myClear();
        //noCusor();       
        txtId.setVisible(false);
        //buttonOff();
    }
    private void buttonOff(){
        btnSave.setText("Save");
        btnSave.setEnabled(false);
        btnEdit.setText("Edit");
        btnEdit.setEnabled(false);
        btnDelete.setEnabled(false);
    }
    private void buttonOn(String b1){
        btnSave.setEnabled(true);
        btnEdit.setEnabled(true);
        btnDelete.setEnabled(true);
        btnSave.setText("Save");
        btnEdit.setText("Edit");
    }
   
   
     private void getNew(){
        if(btnNew.getText()=="New"){
           // btnNew.setIcon(new ImageIcon(.getClass().getResource("book/picture/cancel.jpg")));
            btnNew.setIcon(new ImageIcon(getClass().getResource("Picture\\Cancel.png"))); 
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png")));
            getCusor();
            btnNew.setText("Cancel");
            myClear();
            buttonOff();
            btnSave.setEnabled(true);
        }else if(btnNew.getText()=="Cancel"){
            int a=JOptionPane.showConfirmDialog(null,"Do you want to cancel ?","Comfirm",JOptionPane.YES_NO_OPTION);
            if(JOptionPane.YES_OPTION==a){
            btnNew.setIcon(new ImageIcon(getClass().getResource("Picture\\Folder-Add-icon.png")));
            myClear();
            noCusor();
            btnNew.setText("New");
            btnSave.setEnabled(false);
            }
        }
    }
    private void getEdit(){
        if(btnEdit.getText()=="Edit"){
            getCusor();
            btnSave.setText("Update");
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Cancel.png"))); 
            btnEdit.setText("Cancel");
            btnNew.setText("New");
            btnNew.setIcon(new ImageIcon(getClass().getResource("Picture\\Folder-Add-icon.png")));
            //btnSave.setEnabled(true);
        }else if(btnEdit.getText()=="Cancel"){
            myClear();
            noCusor();
            btnSave.setText("Save");
            btnEdit.setText("Edit");        
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png"))); 
            //buttonOff();
        }
    }
    private void getUpdate(){
        if(btnSave.getText()=="Save"){
            mySave();
            myClear();
        }else if(btnSave.getText()=="Update"){
            myUpdate();
            myClear();
            buttonOff();
            noCusor();
            btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png")));
        }
    }
    
    
    
    
    
    
    
    public void myDelete(){
       // if(!table.getSelectionModel().isSelectionEmpty()) //multi select on table
        int click=JOptionPane.showConfirmDialog(null,"Are you sure want to delete!","Confrim",JOptionPane.YES_NO_OPTION);
        if(click==JOptionPane.YES_OPTION)
        try{
        int id=Integer.parseInt(txtId.getText());
        String sql="Delete from tblroomtype where rtid="+id;
            st=ConnectDB.con.createStatement();
            st.execute(sql);
            JOptionPane.showMessageDialog(null,"Delete Successful");
            int row=table.getSelectedRow();
            model.removeRow(row);
            myClear();
        }catch(SQLException e){e.printStackTrace();}
        
    }
    public void myUpdate(){
        try{
             String query="UPDATE tblRoomType SET rtType = '"+txtType.getText()+"',rtSize='"+txtSize.getText()+"',rtPrice='"+
                                             Double.parseDouble(txtPrice.getText())+"',rtDes='"+txtDes.getText()+"'WHERE rtID="+Integer.parseInt(txtId.getText());
                st=ConnectDB.con.createStatement();
                st.executeUpdate(query);
            JOptionPane.showMessageDialog(null,"Update Successful");
        }catch(SQLException e){e.printStackTrace();}
    }
    private void formWindowOpened(java.awt.event.WindowEvent evt) {                                  
    }                                                          
    public void mouseClick(){
        txtId.setText(model.getValueAt(table.getSelectedRow(),0).toString());
        txtType.setText(model.getValueAt(table.getSelectedRow(),1).toString());
        txtSize.setText(model.getValueAt(table.getSelectedRow(),2).toString());
        txtPrice.setText(model.getValueAt(table.getSelectedRow(),3).toString());
        txtDes.setText(model.getValueAt(table.getSelectedRow(),4).toString());
        btnDelete.setEnabled(true);
        btnEdit.setEnabled(true);
        noCusor();
        //btnSave.setEnabled(false);
        btnNew.setIcon(new ImageIcon(getClass().getResource("Picture\\Cancel.png"))); 
        btnNew.setText("Cancel");
        btnEdit.setText("Edit");        
        btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png"))); 
           
    }
    public void myRandom(){
       Random ra=new Random();
       JOptionPane.showMessageDialog(null,ra.nextInt(87654321));
    }
    public void getCusor(){
        txtType.setFocusable(true);
        txtSize.setFocusable(true);
        txtPrice.setFocusable(true);
        txtDes.setFocusable(true);
        txtType.requestFocus();
    }
    public void noCusor(){
        txtType.setFocusable(false);
        txtSize.setFocusable(false);
        txtPrice.setFocusable(false);
        txtDes.setFocusable(false);
    }
    public void sameName(){
        for(int i=0;i<table.getRowCount();i++){
            String name=(String)model.getValueAt(i,1);
            if(name.equalsIgnoreCase(txtType.getText())){
            JOptionPane.showMessageDialog(null,"Name is exist! Write new Name");
            txtType.setText("");txtType.requestFocus();
            }
        }
    }
    public void mySave(){
        try{
        st=ConnectDB.con.createStatement();
        st.execute("Insert into tblroomtype values('"+0+"','"+
                txtType.getText()+"','"+txtSize.getText()+"','"+Double.parseDouble(txtPrice.getText())+"','"+txtDes.getText()+"')");
        st.close();
       JOptionPane.showMessageDialog(null,"Saved");
        }catch(Exception e){e.printStackTrace();}
    }
    public void initTable(){
        model=new DefaultTableModel();
        table.setModel(model);
        model.addColumn("ID");
        model.addColumn("RoomType");
        model.addColumn("RoomSize");
        model.addColumn("RoomPrice");  
        model.addColumn("Descrition");
        table.getColumnModel().getColumn(1).setPreferredWidth(80);
        table.getColumnModel().getColumn(2).setPreferredWidth(80);
        table.getColumnModel().getColumn(3).setPreferredWidth(80);
        table.getColumnModel().getColumn(4).setPreferredWidth(80);
        
        table.removeColumn(table.getColumnModel().getColumn(0));
        Vector<CRoomType>vec=getDataFromDB();
        for(int i=0;i<vec.size();i++){
            CRoomType r=vec.get(i);
             Object row[]={
                        r.getId(),
                        r.getType(),
                        r.getSize(),
                        r.getPrice(),
                        r.getDes()
        };
            model.addRow(row);
        }
    }
    public void myClear(){
       
        txtType.setText("");
        txtSize.setText("");
        txtPrice.setText("");
        txtDes.setText("");
        txtType.requestFocus();
    }
   
    public Vector<CRoomType> getDataFromDB(){
        Vector<CRoomType>vec=new Vector<CRoomType>();
        CRoomType room;
        try{
            String sql="Select *From tblroomtype order by rtType";
            st=ConnectDB.con.createStatement();
            r=st.executeQuery(sql);
            while(r.next()){
                room=new CRoomType(Integer.parseInt(r.getString("rtID")),r.getString("rtType"),r.getString("rtSize"),
                                    Double.parseDouble(r.getString("rtPrice")),r.getString("rtDes"));
                vec.add(room);
            }
            r.close();
            st.close();
        }catch(SQLException e){e.printStackTrace();}
        return vec;
    }
  
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        txtType = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtSize = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtDes = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtPrice = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        btnEdit = new javax.swing.JButton();
        btnNew = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        btnBack = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        txtSearch = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jPanel3.setBackground(new java.awt.Color(0, 153, 51));

        jPanel2.setBackground(new java.awt.Color(204, 255, 204));

        txtType.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtType.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtTypeKeyPressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("Room Price:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Room Size:");

        txtSize.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtSize.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSizeKeyPressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Room Type:");

        txtDes.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtDes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDesKeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Description:");

        txtPrice.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtPrice.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPriceKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPriceKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5))
                .addGap(46, 46, 46)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtSize)
                            .addComponent(txtDes, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(txtType, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtPrice, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtDes, txtSize, txtType});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtDes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel2, jLabel3, jLabel4, txtDes, txtSize, txtType});

        jPanel1.setBackground(new java.awt.Color(204, 255, 204));

        btnEdit.setBackground(new java.awt.Color(255, 255, 255));
        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/Edit-validated-icon.png"))); // NOI18N
        btnEdit.setText("Edit");
        btnEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnEditMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnEditMouseExited(evt);
            }
        });
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnNew.setBackground(new java.awt.Color(255, 255, 255));
        btnNew.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/Folder-Add-icon.png"))); // NOI18N
        btnNew.setText("New");
        btnNew.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnNewMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnNewMouseExited(evt);
            }
        });
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });

        btnDelete.setBackground(new java.awt.Color(255, 255, 255));
        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/Actions-edit-delete-icon.png"))); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDeleteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnDeleteMouseExited(evt);
            }
        });
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/Save-icon.png"))); // NOI18N
        btnSave.setText("Save");
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnSaveMouseExited(evt);
            }
        });
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnNew)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEdit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDelete)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnDelete, btnEdit, btnNew, btnSave});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNew)
                    .addComponent(btnEdit)
                    .addComponent(btnSave)
                    .addComponent(btnDelete))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnDelete, btnEdit});

        table.setBackground(new java.awt.Color(102, 255, 102));
        table.setForeground(new java.awt.Color(255, 0, 0));
        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        table.setFocusable(false);
        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(table);

        jPanel4.setBackground(new java.awt.Color(255, 204, 255));

        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/back.png"))); // NOI18N
        btnBack.setContentAreaFilled(false);
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 3, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 51, 51));
        jLabel1.setText("RoomType Information ");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 152, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(93, 93, 93)
                .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22))
        );

        txtSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSearch.setForeground(new java.awt.Color(204, 204, 204));
        txtSearch.setText("Search here");
        txtSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtSearch.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtSearchFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtSearchFocusLost(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableMouseClicked
        // TODO add your handling code here:
          mouseClick();
    }//GEN-LAST:event_tableMouseClicked

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        getNew();
    }//GEN-LAST:event_btnNewActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
       if(checkValue()==false){
            getUpdate();
            initTable();
       }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
       getEdit();
    }//GEN-LAST:event_btnEditActionPerformed
    private boolean checkValue(){
         boolean b=false;
        if(txtType.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Please input RoomType ","Missing",JOptionPane.WARNING_MESSAGE);txtType.requestFocus();b=true;
        }else if(txtSize.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Please input RoomType's 'Size'","Missing",JOptionPane.WARNING_MESSAGE);txtSize.requestFocus();b=true;
        }else if(txtPrice.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Please input RoomType's 'Price'","Missing",JOptionPane.WARNING_MESSAGE);txtPrice.requestFocus();b=true;
        }else if(txtDes.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Please input RoomType's 'Description'","Missing",JOptionPane.WARNING_MESSAGE);txtDes.requestFocus();b=true;
         }else b=false;
        return b;
    }
    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        myDelete();
        myClear();
        noCusor();
        buttonOff();
        btnEdit.setIcon(new ImageIcon(getClass().getResource("Picture\\Edit-validated-icon.png")));          
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
          MainForm.main(null);
        this.dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    private void txtSizeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSizeKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER)
            txtPrice.requestFocus();
    }//GEN-LAST:event_txtSizeKeyPressed

    private void txtTypeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTypeKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER)
            txtSize.requestFocus();
    }//GEN-LAST:event_txtTypeKeyPressed

    private void txtPriceKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPriceKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER)
            txtDes.requestFocus();
    }//GEN-LAST:event_txtPriceKeyPressed

    private void txtDesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDesKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER)
            btnSave.doClick();
    }//GEN-LAST:event_txtDesKeyPressed

    private void txtPriceKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPriceKeyTyped
        numberKey(evt,txtPrice);
    }//GEN-LAST:event_txtPriceKeyTyped

    private void txtSearchFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSearchFocusGained
        // TODO add your handling code here:
        txtSearch.setText("");
        //btnNew.setText(setNew);
    }//GEN-LAST:event_txtSearchFocusGained

    private void txtSearchFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSearchFocusLost
        txtSearch.setForeground(Color.DARK_GRAY);
        txtSearch.setText("Search here");
    }//GEN-LAST:event_txtSearchFocusLost

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed
        mySearch();
    }//GEN-LAST:event_txtSearchKeyPressed

    private void btnNewMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNewMouseExited
        btnNew.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnNewMouseExited

    private void btnSaveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseExited
        btnSave.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnSaveMouseExited

    private void btnEditMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditMouseExited
        btnEdit.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnEditMouseExited

    private void btnDeleteMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMouseExited
        btnDelete.setBackground(Color.WHITE);
    }//GEN-LAST:event_btnDeleteMouseExited

    private void btnNewMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNewMouseEntered
        btnNew.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnNewMouseEntered

    private void btnSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseEntered
       btnSave.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnSaveMouseEntered

    private void btnEditMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnEditMouseEntered
        btnEdit.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnEditMouseEntered

    private void btnDeleteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDeleteMouseEntered
       btnDelete.setBackground(Color.GREEN);
    }//GEN-LAST:event_btnDeleteMouseEntered

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try{
            Robot robot=new Robot();
                robot.keyPress(KeyEvent.VK_UP);
        }catch(AWTException e){e.printStackTrace();}
    }//GEN-LAST:event_txtSearchKeyReleased
    private void mySearch(){
        txtSearch.setForeground(Color.BLACK);
        while(model.getRowCount()>0){
            for(int i=0;i<model.getRowCount();i++){
                model.removeRow(i);
            }
        }
        String sql="Select *from tblroomtype where rttype like ?";
        try{
            PreparedStatement pst=ConnectDB.con.prepareStatement(sql);
            pst.setString(1,'%'+txtSearch.getText().trim()+'%');
            r=pst.executeQuery();
            while(r.next()){
                model.addRow(new Object[]{r.getString(1),r.getString(2),r.getString(3),r.getString(4),r.getString(5)});
            }
        }catch(SQLException e){e.printStackTrace();}
    }
    private void numberKey(KeyEvent evt,JTextField txt){
       char c = evt.getKeyChar(); // Get the typed character 
        if (c != KeyEvent.VK_BACK_SPACE && c != KeyEvent.VK_DELETE) {
            if(!(Character.isDigit(c))){
                if ((c == '.')) {
                    if (txt.getText().indexOf(".") < 0)
                        return;
                    else{ 
                        evt.consume(); //Ignore this key
                        return;
                    } 
                } 
                else{
                    evt.consume(); // Ignore this key 
                    return;
                }
            } 
        }
    }
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RoomType().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable table;
    private javax.swing.JTextField txtDes;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtPrice;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtSize;
    private javax.swing.JTextField txtType;
    // End of variables declaration//GEN-END:variables
    private DefaultTableModel model;
}
