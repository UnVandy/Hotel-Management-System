package room;

public class DataConllection {
    private String room;
    private String type;
    private String qty;
    private String price;
    private String prepaid;
    private String amount;

    public DataConllection() {
    }

    public DataConllection(String room, String type,String qty, String price,String prepaid,String amount) {
        this.room = room;
        this.type = type;
        this.qty=qty;
        this.price = price;
        this.prepaid=prepaid;
        this.amount=amount;
    }

    public String getRoom() {
        return room;
    }

    public String getType() {
        return type;
    }
    public String getQty(){
        return qty;
    }

    public String getPrice() {
        return price;
    }
    public String getPrepaid(){
        return prepaid;
    }
    
    public String getAmount() {
        return amount;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setQty(String qty){
        this.qty=qty;
    }
    public void setPrice(String price) {
        this.price = price;
    }
    public void setPreapaid(){
        this.prepaid=prepaid;
    }
    
    public void setAmount(String amount) {
        this.amount = amount;
    }
    
    
    
    
    }
