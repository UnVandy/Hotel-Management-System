package room;

import com.sun.glass.events.KeyEvent;
import java.sql.SQLException;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class User extends javax.swing.JFrame {
    private Statement st;
    private ResultSet rs;
    public User() {
        initComponents();
        setLocationRelativeTo(null);
       // addCombo();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnBack = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        cboName = new javax.swing.JComboBox<>();
        txtID = new javax.swing.JTextField();
        btnCancel = new javax.swing.JButton();
        lblNum9 = new javax.swing.JLabel();
        lblNum4 = new javax.swing.JLabel();
        txtCPass = new javax.swing.JTextField();
        lblNum8 = new javax.swing.JLabel();
        txtPass = new javax.swing.JTextField();
        lblNum5 = new javax.swing.JLabel();
        btnCreate = new javax.swing.JButton();
        lblNum6 = new javax.swing.JLabel();
        txtUsername = new javax.swing.JTextField();
        lblNum7 = new javax.swing.JLabel();
        btnCreate1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/back.png"))); // NOI18N
        btnBack.setContentAreaFilled(false);
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(204, 255, 204));

        cboName.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cboName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboNameActionPerformed(evt);
            }
        });

        txtID.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtID.setFocusable(false);

        btnCancel.setFont(new java.awt.Font("MV Boli", 0, 18)); // NOI18N
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        lblNum9.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNum9.setText("C_Password:");

        lblNum4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNum4.setText("Staff Name:");

        txtCPass.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtCPass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCPassKeyPressed(evt);
            }
        });

        lblNum8.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNum8.setText("Password:");

        txtPass.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtPass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPassKeyPressed(evt);
            }
        });

        lblNum5.setFont(new java.awt.Font("Tekton Pro Ext", 0, 36)); // NOI18N
        lblNum5.setForeground(new java.awt.Color(0, 153, 51));
        lblNum5.setText("Create User");

        btnCreate.setFont(new java.awt.Font("MV Boli", 0, 18)); // NOI18N
        btnCreate.setText("Create");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });

        lblNum6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNum6.setText("Staff ID:");

        txtUsername.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtUsername.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUsernameKeyPressed(evt);
            }
        });

        lblNum7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblNum7.setText("Username:");

        btnCreate1.setFont(new java.awt.Font("MV Boli", 0, 18)); // NOI18N
        btnCreate1.setText("All user");
        btnCreate1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreate1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblNum9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtCPass))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblNum6)
                                    .addComponent(lblNum7)
                                    .addComponent(lblNum8)
                                    .addComponent(lblNum4))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtPass)
                                    .addComponent(cboName, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtID)
                                        .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addComponent(lblNum5))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(btnCreate)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCancel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCreate1)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblNum5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lblNum4)
                                            .addComponent(cboName, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(lblNum6))
                                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblNum7))
                            .addComponent(txtUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblNum8))
                    .addComponent(txtPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblNum9)
                    .addComponent(txtCPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCreate)
                    .addComponent(btnCancel)
                    .addComponent(btnCreate1))
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cboName, txtID});

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pictures/Groups-Meeting-Light-icon.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private ArrayList<String>getstID(){
        //String ar[]=new String[2];
        ArrayList<String>arr=new ArrayList<String>();
        String ab="";
        String sql="SELECT Distinct stname from tbstaff where tbstaff.stID not in (SELECT tbluser.stid FROM tbluser) and StopWork=1";
        try{
            st=ConnectDB.con.createStatement();
            rs=st.executeQuery(sql);
            if(rs.first())
            do{
                ab=rs.getString("stname");
               // ar=new String[]{ab};
               arr.add(ab);
            }while(rs.next());
           // JOptionPane.showMessageDialog(null, arr);
            
        }catch(SQLException e){e.printStackTrace();}
        return arr;
    }
    private void addCombo(){
        ArrayList<String>arr=getstID();
        for(int i=0;i<arr.size();i++){
            String a=arr.get(i);
            cboName.addItem(a);
        }
    }
    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        MainForm.main(null);
        this.dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    private void cboNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboNameActionPerformed
        String sql="Select stid from tbStaff where stname=?";
            try{
                PreparedStatement pst=ConnectDB.con.prepareStatement(sql);
                pst.setString(1,cboName.getSelectedItem().toString());
                    rs=pst.executeQuery();
                    rs.first();
                    txtID.setText(rs.getString("stid"));    
                    txtUsername.requestFocus();
            }catch(SQLException e){e.printStackTrace();}        
    }//GEN-LAST:event_cboNameActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        addCombo();
    }//GEN-LAST:event_formWindowOpened

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
        if(txtUsername.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Username is null","Missing",JOptionPane.WARNING_MESSAGE);
            txtUsername.requestFocus();
        }else if(txtPass.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Password is null","Missing",JOptionPane.WARNING_MESSAGE);
            txtPass.requestFocus();
        }else if(txtCPass.getText().trim().equals("")){
            JOptionPane.showMessageDialog(null,"Confirm Password is null","Missing",JOptionPane.WARNING_MESSAGE);
            txtCPass.requestFocus();
        }else{
            myCreate();
            this.dispose();
            User.main(null);
        }
    }//GEN-LAST:event_btnCreateActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
       myClear();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void txtUsernameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUsernameKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER)
            txtPass.requestFocus();
    }//GEN-LAST:event_txtUsernameKeyPressed

    private void txtPassKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPassKeyPressed
        if(evt.getKeyCode()==10)
            txtCPass.requestFocus();
    }//GEN-LAST:event_txtPassKeyPressed

    private void txtCPassKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCPassKeyPressed
        if(evt.getKeyCode()==10)
            btnCreate.doClick();
    }//GEN-LAST:event_txtCPassKeyPressed

    private void btnCreate1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreate1ActionPerformed
        ShowUser.main(null);
        this.dispose();
    }//GEN-LAST:event_btnCreate1ActionPerformed
    public void myClear(){
        txtID.setText("");
        txtUsername.setText("");
        txtPass.setText("");
        txtCPass.setText("");
    }
    public void myCreate(){
        String sql="Insert into tblUser(stid,username,password) values(?,?,?)";
        if(txtPass.getText().equalsIgnoreCase(txtCPass.getText())){              
            try{
                PreparedStatement pst=ConnectDB.con.prepareStatement(sql);
                pst.setInt(1,Integer.parseInt(txtID.getText()));
                pst.setString(2,txtUsername.getText());
                pst.setString(3,txtPass.getText());
                pst.executeUpdate();
                myClear();
                JOptionPane.showMessageDialog(null,"Create successfully");
            }catch(SQLException e){e.printStackTrace();}        
        }else{ 
            JOptionPane.showMessageDialog(null,"Confirm Password Incorrect!");
            txtCPass.requestFocus();
        }
    }
        
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new User().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnCreate;
    private javax.swing.JButton btnCreate1;
    private javax.swing.JComboBox<String> cboName;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblNum4;
    private javax.swing.JLabel lblNum5;
    private javax.swing.JLabel lblNum6;
    private javax.swing.JLabel lblNum7;
    private javax.swing.JLabel lblNum8;
    private javax.swing.JLabel lblNum9;
    private javax.swing.JTextField txtCPass;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtPass;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables
}
