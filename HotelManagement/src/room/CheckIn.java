package room;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Vector;
import java.util.logging.Level;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;


public class CheckIn extends javax.swing.JFrame {
    Statement st;
    ResultSet r;
    private JScrollPane jsp;
    Guest g=new Guest();
    public CheckIn() {
        initComponents();
        setLocationRelativeTo(null);
       initTable();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jLabel20 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        txtSearch = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(102, 255, 102));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/room/Picture/back.png"))); // NOI18N
        jButton1.setBorderPainted(false);
        jButton1.setContentAreaFilled(false);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Segoe Print", 3, 36)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(153, 0, 0));
        jLabel20.setText("CheckIn Customer");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(203, 203, 203)
                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 342, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 8, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(204, 204, 255));

        jScrollPane3.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane3.setViewportView(table);

        txtSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSearch.setForeground(new java.awt.Color(204, 204, 204));
        txtSearch.setText("Search name,contact,card");
        txtSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        txtSearch.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtSearchFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtSearchFocusLost(evt);
            }
        });
        txtSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSearchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSearchKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 828, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 182, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    public void addTable(){
       java.util.Date cd=new java.util.Date();
            String d=new SimpleDateFormat("dd").format(cd);
            String m=new SimpleDateFormat("MM").format(cd);
            String y=new SimpleDateFormat("yyyy").format(cd);   
       String sql="Select *from vguestdetail where checkin<='"+y+"-"+m+"-"+d+"' and checkout>='"+y+"-"+m+"-"+d+"' order by guname"; 
      table(sql);
   }
    
    public void initTable(){
        model.addColumn("G_ID");
        model.addColumn("Name");
        //model.addColumn("rtType");
        model.addColumn("Identify Card");
        model.addColumn("Contact");
        model.addColumn("Address");
        model.addColumn("Room Name");
        model.addColumn("Room Type");
        model.addColumn("Size");
        model.addColumn("Price");
        model.addColumn("Floor");
        model.addColumn("CheckIn");
        model.addColumn("CheckOut");
        model.addColumn("Day");
        model.addColumn("Prepaid");
        model.addColumn("Total");
        model.addColumn("Invoice");
        table.setModel(model);
      //  table.getColumnModel().getColumn(0).setPreferredWidth(40);
        table.getColumnModel().getColumn(1).setPreferredWidth(120);
        table.getColumnModel().getColumn(2).setPreferredWidth(80);
        table.getColumnModel().getColumn(3).setPreferredWidth(80);
        table.getColumnModel().getColumn(4).setPreferredWidth(90);
        table.getColumnModel().getColumn(5).setPreferredWidth(85);
        table.getColumnModel().getColumn(6).setPreferredWidth(80);
        table.getColumnModel().getColumn(7).setPreferredWidth(60);
        table.getColumnModel().getColumn(8).setPreferredWidth(70);
        table.getColumnModel().getColumn(9).setPreferredWidth(60);
        table.getColumnModel().getColumn(10).setPreferredWidth(80);
        table.getColumnModel().getColumn(11).setPreferredWidth(80);
        table.getColumnModel().getColumn(12).setPreferredWidth(50);
        table.getColumnModel().getColumn(13).setPreferredWidth(70);
        table.getColumnModel().getColumn(14).setPreferredWidth(70);
        table.getColumnModel().getColumn(15).setPreferredWidth(80);
        //table.removeColumn(table.getColumnModel().getColumn(15));
        table.removeColumn(table.getColumnModel().getColumn(0));
        addTable();
    }
   public void table(String sql){      
        try{
        st=ConnectDB.con.createStatement();
        r=st.executeQuery(sql);
        while(r.next()){
          //  JOptionPane.showMessageDialog(null,r.getString("guName"));
             model.addRow(new Object[]{r.getString("guID"),r.getString("guName"),r.getString("idc"),r.getString("contact"),
                        r.getString("guadd"),r.getString("rname"),r.getString("rttype"),
                        r.getString("rtsize"),r.getString("rtprice"),r.getString("rtDes"),r.getString("checkin"),r.getString("checkout"),r.getString("Dns"),
                        r.getString("prepaid"),r.getString("amount"),r.getString("idran")
                 });
        }
        }catch(SQLException e){e.printStackTrace();}    
    }
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        MainForm.main(null);
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtSearchFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSearchFocusGained
        // TODO add your handling code here:
        txtSearch.setText("");
    }//GEN-LAST:event_txtSearchFocusGained

    private void txtSearchFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSearchFocusLost
        txtSearch.setForeground(Color.DARK_GRAY);
        txtSearch.setText("Search name,contact,card");
    }//GEN-LAST:event_txtSearchFocusLost

    private void txtSearchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyPressed

        txtSearch.setForeground(Color.BLACK);
        while(model.getRowCount()>0){
            for(int j=0;j<model.getRowCount();j++)
            model.removeRow(j);
        }
        try{
            String sql="Select *From vguestdetail where (guname Like ? || contact Like ? || idc Like ? || idran Like ?) and rstatue=?";

            PreparedStatement pst=ConnectDB.con.prepareStatement(sql);
            pst.setString(1,"%"+txtSearch.getText()+"%");
            pst.setString(2,"%"+txtSearch.getText()+"%");
            pst.setString(3,"%"+txtSearch.getText()+"%");
            pst.setString(4,"%"+txtSearch.getText()+"%");
            pst.setString(5,"Checkin");
            r=pst.executeQuery();
            CGuest gu;
            while(r.next()){
                gu=new CGuest(Integer.parseInt(r.getString("guID")),r.getString("guName"),r.getString("rtType"),r.getString("rName"),
                    Double.parseDouble(r.getString("rtPrice")),r.getString("rtDes"),r.getString("rtSize"),r.getString("checkin"),
                    r.getString("checkout"),r.getString("contact"),r.getString("idc"),r.getString("guAdd"),Integer.parseInt(r.getString("dns")),
                    Double.parseDouble(r.getString("prepaid")),Double.parseDouble(r.getString("amount")),r.getString("idran"));
                Object row[]={
                    gu.getId(),
                    gu.getName(),
                    gu.getCard(),
                    gu.getContact(),
                    gu.getAdd(),
                    gu.getRName(),//room name
                    gu.getRttype(),
                    gu.getSize(),
                    gu.getPrice(),
                    gu.getDes(),//room descrition
                    gu.getChin(),
                    gu.getChout(),
                    gu.getDns(),
                    gu.getPrepaid(),
                    gu.getAmount(),
                    gu.getIdran()
                };
                model.addRow(row);
            }
        }catch(SQLException e){e.printStackTrace();}

    }//GEN-LAST:event_txtSearchKeyPressed

    private void txtSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSearchKeyReleased
        try {
            Robot robot=new Robot();
            robot.keyPress(KeyEvent.VK_UP);
        } catch (AWTException ex) {
            ex.printStackTrace();
        }

    }//GEN-LAST:event_txtSearchKeyReleased
 
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CheckIn().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable table;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
    private DefaultTableModel model=new DefaultTableModel();
}
